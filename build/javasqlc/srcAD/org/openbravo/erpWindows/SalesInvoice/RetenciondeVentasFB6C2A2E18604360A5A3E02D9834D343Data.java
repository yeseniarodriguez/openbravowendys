//Sqlc generated V1.O00-1
package org.openbravo.erpWindows.SalesInvoice;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import java.util.*;

/**
WAD Generated class
 */
class RetenciondeVentasFB6C2A2E18604360A5A3E02D9834D343Data implements FieldProvider {
static Logger log4j = Logger.getLogger(RetenciondeVentasFB6C2A2E18604360A5A3E02D9834D343Data.class);
  private String InitRecordNumber="0";
  public String created;
  public String createdbyr;
  public String updated;
  public String updatedTimeStamp;
  public String updatedby;
  public String updatedbyr;
  public String adOrgId;
  public String coNoEstablecimiento;
  public String coPtoEmision;
  public String noAutorizacion;
  public String cDoctypeId;
  public String cDoctypeIdr;
  public String documentno;
  public String fechaEmision;
  public String tipoComprobanteVenta;
  public String tipoComprobanteVentar;
  public String noComprobanteVenta;
  public String dateacct;
  public String totalRetencion;
  public String processed;
  public String isactive;
  public String posted;
  public String postedBtn;
  public String docactionre;
  public String docactionreBtn;
  public String processing;
  public String docstatus;
  public String cDoctypetargetId;
  public String coRetencionVentaId;
  public String cInvoiceId;
  public String adClientId;
  public String language;
  public String adUserClient;
  public String adOrgClient;
  public String createdby;
  public String trBgcolor;
  public String totalCount;
  public String dateTimeFormat;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("created"))
      return created;
    else if (fieldName.equalsIgnoreCase("createdbyr"))
      return createdbyr;
    else if (fieldName.equalsIgnoreCase("updated"))
      return updated;
    else if (fieldName.equalsIgnoreCase("updated_time_stamp") || fieldName.equals("updatedTimeStamp"))
      return updatedTimeStamp;
    else if (fieldName.equalsIgnoreCase("updatedby"))
      return updatedby;
    else if (fieldName.equalsIgnoreCase("updatedbyr"))
      return updatedbyr;
    else if (fieldName.equalsIgnoreCase("ad_org_id") || fieldName.equals("adOrgId"))
      return adOrgId;
    else if (fieldName.equalsIgnoreCase("co_no_establecimiento") || fieldName.equals("coNoEstablecimiento"))
      return coNoEstablecimiento;
    else if (fieldName.equalsIgnoreCase("co_pto_emision") || fieldName.equals("coPtoEmision"))
      return coPtoEmision;
    else if (fieldName.equalsIgnoreCase("no_autorizacion") || fieldName.equals("noAutorizacion"))
      return noAutorizacion;
    else if (fieldName.equalsIgnoreCase("c_doctype_id") || fieldName.equals("cDoctypeId"))
      return cDoctypeId;
    else if (fieldName.equalsIgnoreCase("c_doctype_idr") || fieldName.equals("cDoctypeIdr"))
      return cDoctypeIdr;
    else if (fieldName.equalsIgnoreCase("documentno"))
      return documentno;
    else if (fieldName.equalsIgnoreCase("fecha_emision") || fieldName.equals("fechaEmision"))
      return fechaEmision;
    else if (fieldName.equalsIgnoreCase("tipo_comprobante_venta") || fieldName.equals("tipoComprobanteVenta"))
      return tipoComprobanteVenta;
    else if (fieldName.equalsIgnoreCase("tipo_comprobante_ventar") || fieldName.equals("tipoComprobanteVentar"))
      return tipoComprobanteVentar;
    else if (fieldName.equalsIgnoreCase("no_comprobante_venta") || fieldName.equals("noComprobanteVenta"))
      return noComprobanteVenta;
    else if (fieldName.equalsIgnoreCase("dateacct"))
      return dateacct;
    else if (fieldName.equalsIgnoreCase("total_retencion") || fieldName.equals("totalRetencion"))
      return totalRetencion;
    else if (fieldName.equalsIgnoreCase("processed"))
      return processed;
    else if (fieldName.equalsIgnoreCase("isactive"))
      return isactive;
    else if (fieldName.equalsIgnoreCase("posted"))
      return posted;
    else if (fieldName.equalsIgnoreCase("posted_btn") || fieldName.equals("postedBtn"))
      return postedBtn;
    else if (fieldName.equalsIgnoreCase("docactionre"))
      return docactionre;
    else if (fieldName.equalsIgnoreCase("docactionre_btn") || fieldName.equals("docactionreBtn"))
      return docactionreBtn;
    else if (fieldName.equalsIgnoreCase("processing"))
      return processing;
    else if (fieldName.equalsIgnoreCase("docstatus"))
      return docstatus;
    else if (fieldName.equalsIgnoreCase("c_doctypetarget_id") || fieldName.equals("cDoctypetargetId"))
      return cDoctypetargetId;
    else if (fieldName.equalsIgnoreCase("co_retencion_venta_id") || fieldName.equals("coRetencionVentaId"))
      return coRetencionVentaId;
    else if (fieldName.equalsIgnoreCase("c_invoice_id") || fieldName.equals("cInvoiceId"))
      return cInvoiceId;
    else if (fieldName.equalsIgnoreCase("ad_client_id") || fieldName.equals("adClientId"))
      return adClientId;
    else if (fieldName.equalsIgnoreCase("language"))
      return language;
    else if (fieldName.equals("adUserClient"))
      return adUserClient;
    else if (fieldName.equals("adOrgClient"))
      return adOrgClient;
    else if (fieldName.equals("createdby"))
      return createdby;
    else if (fieldName.equals("trBgcolor"))
      return trBgcolor;
    else if (fieldName.equals("totalCount"))
      return totalCount;
    else if (fieldName.equals("dateTimeFormat"))
      return dateTimeFormat;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

/**
Select for edit
 */
  public static RetenciondeVentasFB6C2A2E18604360A5A3E02D9834D343Data[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String cInvoiceId, String key, String adUserClient, String adOrgClient)    throws ServletException {
    return selectEdit(connectionProvider, dateTimeFormat, paramLanguage, cInvoiceId, key, adUserClient, adOrgClient, 0, 0);
  }

/**
Select for edit
 */
  public static RetenciondeVentasFB6C2A2E18604360A5A3E02D9834D343Data[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String cInvoiceId, String key, String adUserClient, String adOrgClient, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(CO_RETENCION_VENTA.Created, ?) as created, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = CO_RETENCION_VENTA.CreatedBy) as CreatedByR, " +
      "        to_char(CO_RETENCION_VENTA.Updated, ?) as updated, " +
      "        to_char(CO_RETENCION_VENTA.Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp,  " +
      "        CO_RETENCION_VENTA.UpdatedBy, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = CO_RETENCION_VENTA.UpdatedBy) as UpdatedByR," +
      "        CO_RETENCION_VENTA.AD_Org_ID, " +
      "CO_RETENCION_VENTA.CO_No_Establecimiento, " +
      "CO_RETENCION_VENTA.CO_Pto_Emision, " +
      "CO_RETENCION_VENTA.NO_Autorizacion, " +
      "CO_RETENCION_VENTA.C_Doctype_ID, " +
      "(CASE WHEN CO_RETENCION_VENTA.C_Doctype_ID IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR((CASE WHEN tableTRL1.Name IS NULL THEN TO_CHAR(table1.Name) ELSE TO_CHAR(tableTRL1.Name) END)), ''))),'') ) END) AS C_Doctype_IDR, " +
      "CO_RETENCION_VENTA.DocumentNo, " +
      "CO_RETENCION_VENTA.Fecha_Emision, " +
      "CO_RETENCION_VENTA.Tipo_Comprobante_Venta, " +
      "(CASE WHEN CO_RETENCION_VENTA.Tipo_Comprobante_Venta IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(list1.name),'') ) END) AS Tipo_Comprobante_VentaR, " +
      "CO_RETENCION_VENTA.NO_Comprobante_Venta, " +
      "CO_RETENCION_VENTA.Dateacct, " +
      "CO_RETENCION_VENTA.Total_Retencion, " +
      "COALESCE(CO_RETENCION_VENTA.Processed, 'N') AS Processed, " +
      "COALESCE(CO_RETENCION_VENTA.Isactive, 'N') AS Isactive, " +
      "CO_RETENCION_VENTA.Posted, " +
      "list2.name as Posted_BTN, " +
      "CO_RETENCION_VENTA.Docactionre, " +
      "list3.name as Docactionre_BTN, " +
      "CO_RETENCION_VENTA.Processing, " +
      "CO_RETENCION_VENTA.Docstatus, " +
      "CO_RETENCION_VENTA.C_Doctypetarget_ID, " +
      "CO_RETENCION_VENTA.CO_Retencion_Venta_ID, " +
      "CO_RETENCION_VENTA.C_Invoice_ID, " +
      "CO_RETENCION_VENTA.AD_Client_ID, " +
      "        ? AS LANGUAGE " +
      "        FROM CO_RETENCION_VENTA left join (select C_DocType_ID, Name from C_DocType) table1 on (CO_RETENCION_VENTA.C_Doctype_ID =  table1.C_DocType_ID) left join (select C_DocType_ID,AD_Language, Name from C_DocType_TRL) tableTRL1 on (table1.C_DocType_ID = tableTRL1.C_DocType_ID and tableTRL1.AD_Language = ?)  left join ad_ref_list_v list1 on (CO_RETENCION_VENTA.Tipo_Comprobante_Venta = list1.value and list1.ad_reference_id = '94DD3D9C266148BEAE4E201BD84F8F76' and list1.ad_language = ?)  left join ad_ref_list_v list2 on (list2.ad_reference_id = '234' and list2.ad_language = ?  AND CO_RETENCION_VENTA.Posted = TO_CHAR(list2.value)) left join ad_ref_list_v list3 on (list3.ad_reference_id = 'CB29EF103ACC49108693B711ACEF6261' and list3.ad_language = ?  AND CO_RETENCION_VENTA.Docactionre = TO_CHAR(list3.value))" +
      "        WHERE 2=2 " +
      "        AND 1=1 ";
    strSql = strSql + ((cInvoiceId==null || cInvoiceId.equals(""))?"":"  AND CO_RETENCION_VENTA.C_Invoice_ID = ?  ");
    strSql = strSql + 
      "        AND CO_RETENCION_VENTA.CO_Retencion_Venta_ID = ? " +
      "        AND CO_RETENCION_VENTA.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "           AND CO_RETENCION_VENTA.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      if (cInvoiceId != null && !(cInvoiceId.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, cInvoiceId);
      }
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, key);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        RetenciondeVentasFB6C2A2E18604360A5A3E02D9834D343Data objectRetenciondeVentasFB6C2A2E18604360A5A3E02D9834D343Data = new RetenciondeVentasFB6C2A2E18604360A5A3E02D9834D343Data();
        objectRetenciondeVentasFB6C2A2E18604360A5A3E02D9834D343Data.created = UtilSql.getValue(result, "created");
        objectRetenciondeVentasFB6C2A2E18604360A5A3E02D9834D343Data.createdbyr = UtilSql.getValue(result, "createdbyr");
        objectRetenciondeVentasFB6C2A2E18604360A5A3E02D9834D343Data.updated = UtilSql.getValue(result, "updated");
        objectRetenciondeVentasFB6C2A2E18604360A5A3E02D9834D343Data.updatedTimeStamp = UtilSql.getValue(result, "updated_time_stamp");
        objectRetenciondeVentasFB6C2A2E18604360A5A3E02D9834D343Data.updatedby = UtilSql.getValue(result, "updatedby");
        objectRetenciondeVentasFB6C2A2E18604360A5A3E02D9834D343Data.updatedbyr = UtilSql.getValue(result, "updatedbyr");
        objectRetenciondeVentasFB6C2A2E18604360A5A3E02D9834D343Data.adOrgId = UtilSql.getValue(result, "ad_org_id");
        objectRetenciondeVentasFB6C2A2E18604360A5A3E02D9834D343Data.coNoEstablecimiento = UtilSql.getValue(result, "co_no_establecimiento");
        objectRetenciondeVentasFB6C2A2E18604360A5A3E02D9834D343Data.coPtoEmision = UtilSql.getValue(result, "co_pto_emision");
        objectRetenciondeVentasFB6C2A2E18604360A5A3E02D9834D343Data.noAutorizacion = UtilSql.getValue(result, "no_autorizacion");
        objectRetenciondeVentasFB6C2A2E18604360A5A3E02D9834D343Data.cDoctypeId = UtilSql.getValue(result, "c_doctype_id");
        objectRetenciondeVentasFB6C2A2E18604360A5A3E02D9834D343Data.cDoctypeIdr = UtilSql.getValue(result, "c_doctype_idr");
        objectRetenciondeVentasFB6C2A2E18604360A5A3E02D9834D343Data.documentno = UtilSql.getValue(result, "documentno");
        objectRetenciondeVentasFB6C2A2E18604360A5A3E02D9834D343Data.fechaEmision = UtilSql.getDateValue(result, "fecha_emision", "dd-MM-yyyy");
        objectRetenciondeVentasFB6C2A2E18604360A5A3E02D9834D343Data.tipoComprobanteVenta = UtilSql.getValue(result, "tipo_comprobante_venta");
        objectRetenciondeVentasFB6C2A2E18604360A5A3E02D9834D343Data.tipoComprobanteVentar = UtilSql.getValue(result, "tipo_comprobante_ventar");
        objectRetenciondeVentasFB6C2A2E18604360A5A3E02D9834D343Data.noComprobanteVenta = UtilSql.getValue(result, "no_comprobante_venta");
        objectRetenciondeVentasFB6C2A2E18604360A5A3E02D9834D343Data.dateacct = UtilSql.getDateValue(result, "dateacct", "dd-MM-yyyy");
        objectRetenciondeVentasFB6C2A2E18604360A5A3E02D9834D343Data.totalRetencion = UtilSql.getValue(result, "total_retencion");
        objectRetenciondeVentasFB6C2A2E18604360A5A3E02D9834D343Data.processed = UtilSql.getValue(result, "processed");
        objectRetenciondeVentasFB6C2A2E18604360A5A3E02D9834D343Data.isactive = UtilSql.getValue(result, "isactive");
        objectRetenciondeVentasFB6C2A2E18604360A5A3E02D9834D343Data.posted = UtilSql.getValue(result, "posted");
        objectRetenciondeVentasFB6C2A2E18604360A5A3E02D9834D343Data.postedBtn = UtilSql.getValue(result, "posted_btn");
        objectRetenciondeVentasFB6C2A2E18604360A5A3E02D9834D343Data.docactionre = UtilSql.getValue(result, "docactionre");
        objectRetenciondeVentasFB6C2A2E18604360A5A3E02D9834D343Data.docactionreBtn = UtilSql.getValue(result, "docactionre_btn");
        objectRetenciondeVentasFB6C2A2E18604360A5A3E02D9834D343Data.processing = UtilSql.getValue(result, "processing");
        objectRetenciondeVentasFB6C2A2E18604360A5A3E02D9834D343Data.docstatus = UtilSql.getValue(result, "docstatus");
        objectRetenciondeVentasFB6C2A2E18604360A5A3E02D9834D343Data.cDoctypetargetId = UtilSql.getValue(result, "c_doctypetarget_id");
        objectRetenciondeVentasFB6C2A2E18604360A5A3E02D9834D343Data.coRetencionVentaId = UtilSql.getValue(result, "co_retencion_venta_id");
        objectRetenciondeVentasFB6C2A2E18604360A5A3E02D9834D343Data.cInvoiceId = UtilSql.getValue(result, "c_invoice_id");
        objectRetenciondeVentasFB6C2A2E18604360A5A3E02D9834D343Data.adClientId = UtilSql.getValue(result, "ad_client_id");
        objectRetenciondeVentasFB6C2A2E18604360A5A3E02D9834D343Data.language = UtilSql.getValue(result, "language");
        objectRetenciondeVentasFB6C2A2E18604360A5A3E02D9834D343Data.adUserClient = "";
        objectRetenciondeVentasFB6C2A2E18604360A5A3E02D9834D343Data.adOrgClient = "";
        objectRetenciondeVentasFB6C2A2E18604360A5A3E02D9834D343Data.createdby = "";
        objectRetenciondeVentasFB6C2A2E18604360A5A3E02D9834D343Data.trBgcolor = "";
        objectRetenciondeVentasFB6C2A2E18604360A5A3E02D9834D343Data.totalCount = "";
        objectRetenciondeVentasFB6C2A2E18604360A5A3E02D9834D343Data.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectRetenciondeVentasFB6C2A2E18604360A5A3E02D9834D343Data);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    RetenciondeVentasFB6C2A2E18604360A5A3E02D9834D343Data objectRetenciondeVentasFB6C2A2E18604360A5A3E02D9834D343Data[] = new RetenciondeVentasFB6C2A2E18604360A5A3E02D9834D343Data[vector.size()];
    vector.copyInto(objectRetenciondeVentasFB6C2A2E18604360A5A3E02D9834D343Data);
    return(objectRetenciondeVentasFB6C2A2E18604360A5A3E02D9834D343Data);
  }

/**
Create a registry
 */
  public static RetenciondeVentasFB6C2A2E18604360A5A3E02D9834D343Data[] set(String cInvoiceId, String cDoctypetargetId, String processed, String docstatus, String coNoEstablecimiento, String createdby, String createdbyr, String fechaEmision, String posted, String postedBtn, String dateacct, String noComprobanteVenta, String updatedby, String updatedbyr, String isactive, String documentno, String docactionre, String docactionreBtn, String adOrgId, String processing, String adClientId, String coRetencionVentaId, String tipoComprobanteVenta, String coPtoEmision, String totalRetencion, String noAutorizacion, String cDoctypeId)    throws ServletException {
    RetenciondeVentasFB6C2A2E18604360A5A3E02D9834D343Data objectRetenciondeVentasFB6C2A2E18604360A5A3E02D9834D343Data[] = new RetenciondeVentasFB6C2A2E18604360A5A3E02D9834D343Data[1];
    objectRetenciondeVentasFB6C2A2E18604360A5A3E02D9834D343Data[0] = new RetenciondeVentasFB6C2A2E18604360A5A3E02D9834D343Data();
    objectRetenciondeVentasFB6C2A2E18604360A5A3E02D9834D343Data[0].created = "";
    objectRetenciondeVentasFB6C2A2E18604360A5A3E02D9834D343Data[0].createdbyr = createdbyr;
    objectRetenciondeVentasFB6C2A2E18604360A5A3E02D9834D343Data[0].updated = "";
    objectRetenciondeVentasFB6C2A2E18604360A5A3E02D9834D343Data[0].updatedTimeStamp = "";
    objectRetenciondeVentasFB6C2A2E18604360A5A3E02D9834D343Data[0].updatedby = updatedby;
    objectRetenciondeVentasFB6C2A2E18604360A5A3E02D9834D343Data[0].updatedbyr = updatedbyr;
    objectRetenciondeVentasFB6C2A2E18604360A5A3E02D9834D343Data[0].adOrgId = adOrgId;
    objectRetenciondeVentasFB6C2A2E18604360A5A3E02D9834D343Data[0].coNoEstablecimiento = coNoEstablecimiento;
    objectRetenciondeVentasFB6C2A2E18604360A5A3E02D9834D343Data[0].coPtoEmision = coPtoEmision;
    objectRetenciondeVentasFB6C2A2E18604360A5A3E02D9834D343Data[0].noAutorizacion = noAutorizacion;
    objectRetenciondeVentasFB6C2A2E18604360A5A3E02D9834D343Data[0].cDoctypeId = cDoctypeId;
    objectRetenciondeVentasFB6C2A2E18604360A5A3E02D9834D343Data[0].cDoctypeIdr = "";
    objectRetenciondeVentasFB6C2A2E18604360A5A3E02D9834D343Data[0].documentno = documentno;
    objectRetenciondeVentasFB6C2A2E18604360A5A3E02D9834D343Data[0].fechaEmision = fechaEmision;
    objectRetenciondeVentasFB6C2A2E18604360A5A3E02D9834D343Data[0].tipoComprobanteVenta = tipoComprobanteVenta;
    objectRetenciondeVentasFB6C2A2E18604360A5A3E02D9834D343Data[0].tipoComprobanteVentar = "";
    objectRetenciondeVentasFB6C2A2E18604360A5A3E02D9834D343Data[0].noComprobanteVenta = noComprobanteVenta;
    objectRetenciondeVentasFB6C2A2E18604360A5A3E02D9834D343Data[0].dateacct = dateacct;
    objectRetenciondeVentasFB6C2A2E18604360A5A3E02D9834D343Data[0].totalRetencion = totalRetencion;
    objectRetenciondeVentasFB6C2A2E18604360A5A3E02D9834D343Data[0].processed = processed;
    objectRetenciondeVentasFB6C2A2E18604360A5A3E02D9834D343Data[0].isactive = isactive;
    objectRetenciondeVentasFB6C2A2E18604360A5A3E02D9834D343Data[0].posted = posted;
    objectRetenciondeVentasFB6C2A2E18604360A5A3E02D9834D343Data[0].postedBtn = postedBtn;
    objectRetenciondeVentasFB6C2A2E18604360A5A3E02D9834D343Data[0].docactionre = docactionre;
    objectRetenciondeVentasFB6C2A2E18604360A5A3E02D9834D343Data[0].docactionreBtn = docactionreBtn;
    objectRetenciondeVentasFB6C2A2E18604360A5A3E02D9834D343Data[0].processing = processing;
    objectRetenciondeVentasFB6C2A2E18604360A5A3E02D9834D343Data[0].docstatus = docstatus;
    objectRetenciondeVentasFB6C2A2E18604360A5A3E02D9834D343Data[0].cDoctypetargetId = cDoctypetargetId;
    objectRetenciondeVentasFB6C2A2E18604360A5A3E02D9834D343Data[0].coRetencionVentaId = coRetencionVentaId;
    objectRetenciondeVentasFB6C2A2E18604360A5A3E02D9834D343Data[0].cInvoiceId = cInvoiceId;
    objectRetenciondeVentasFB6C2A2E18604360A5A3E02D9834D343Data[0].adClientId = adClientId;
    objectRetenciondeVentasFB6C2A2E18604360A5A3E02D9834D343Data[0].language = "";
    return objectRetenciondeVentasFB6C2A2E18604360A5A3E02D9834D343Data;
  }

/**
Select for auxiliar field
 */
  public static String selectDef180DACC7A8C64503904FDC79FAEC513E(ConnectionProvider connectionProvider, String c_invoice_id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        select COALESCE(em_co_bp_nro_estab,'001') AS DefaultValue from c_bpartner where c_bpartner_id in (select c_bpartner_id from c_invoice where c_invoice_id = ?) ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, c_invoice_id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "defaultvalue");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDef28707085D03D4CA593A06A750114DEEE_0(ConnectionProvider connectionProvider, String CreatedbyR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as Createdby FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, CreatedbyR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "createdby");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDef399EF229625B47D890FE7831F76DB253(ConnectionProvider connectionProvider, String AD_CLIENT_ID, String c_invoice_id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        select coalesce(em_co_nro_estab,'') || '-' || coalesce(em_co_punto_emision,'') || '-' || documentno as defaultValue from c_invoice where ad_client_id = ? and c_invoice_id = ? ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, AD_CLIENT_ID);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, c_invoice_id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "defaultvalue");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDef4D49B91918F34C47854FE88F1A8C4FAD_1(ConnectionProvider connectionProvider, String UpdatedbyR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as Updatedby FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, UpdatedbyR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "updatedby");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDefEAB8C210B6D541E68296959AC3104EE1(ConnectionProvider connectionProvider, String c_invoice_id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        select COALESCE(em_co_bp_punto_emision,'001') AS DefaultValue from c_bpartner where c_bpartner_id in (select c_bpartner_id from c_invoice where c_invoice_id = ?) ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, c_invoice_id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "defaultvalue");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDefFA8A7F8EBC774ABFBCF7B50F8719DFA0(ConnectionProvider connectionProvider, String c_invoice_id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        select COALESCE(em_co_bp_nro_aut_rt_sri,'') AS DefaultValue from c_bpartner  where c_bpartner_id in (select c_bpartner_id from c_invoice where c_invoice_id = ?) ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, c_invoice_id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "defaultvalue");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
return the parent ID
 */
  public static String selectParentID(ConnectionProvider connectionProvider, String key)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT CO_RETENCION_VENTA.C_Invoice_ID AS NAME" +
      "        FROM CO_RETENCION_VENTA" +
      "        WHERE CO_RETENCION_VENTA.CO_Retencion_Venta_ID = ?";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, key);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "name");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for parent field
 */
  public static String selectParent(ConnectionProvider connectionProvider, String cInvoiceId)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT (TO_CHAR(COALESCE(TO_CHAR(table1.DocumentNo), '')) || ' - ' || TO_CHAR(table1.DateInvoiced, 'DD-MM-YYYY') || ' - ' || TO_CHAR(COALESCE(TO_CHAR(table1.GrandTotal), ''))) AS NAME FROM C_Invoice left join (select C_Invoice_ID, DocumentNo, DateInvoiced, GrandTotal from C_Invoice) table1 on (C_Invoice.C_Invoice_ID = table1.C_Invoice_ID) WHERE C_Invoice.C_Invoice_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cInvoiceId);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "name");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for parent field
 */
  public static String selectParentTrl(ConnectionProvider connectionProvider, String cInvoiceId)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT (TO_CHAR(COALESCE(TO_CHAR(table1.DocumentNo), '')) || ' - ' || TO_CHAR(table1.DateInvoiced, 'DD-MM-YYYY') || ' - ' || TO_CHAR(COALESCE(TO_CHAR(table1.GrandTotal), ''))) AS NAME FROM C_Invoice left join (select C_Invoice_ID, DocumentNo, DateInvoiced, GrandTotal from C_Invoice) table1 on (C_Invoice.C_Invoice_ID = table1.C_Invoice_ID) WHERE C_Invoice.C_Invoice_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cInvoiceId);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "name");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public int update(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        UPDATE CO_RETENCION_VENTA" +
      "        SET AD_Org_ID = (?) , CO_No_Establecimiento = (?) , CO_Pto_Emision = (?) , NO_Autorizacion = (?) , C_Doctype_ID = (?) , DocumentNo = (?) , Fecha_Emision = TO_DATE(?) , Tipo_Comprobante_Venta = (?) , NO_Comprobante_Venta = (?) , Dateacct = TO_DATE(?) , Total_Retencion = TO_NUMBER(?) , Processed = (?) , Isactive = (?) , Posted = (?) , Docactionre = (?) , Processing = (?) , Docstatus = (?) , C_Doctypetarget_ID = (?) , CO_Retencion_Venta_ID = (?) , C_Invoice_ID = (?) , AD_Client_ID = (?) , updated = now(), updatedby = ? " +
      "        WHERE CO_RETENCION_VENTA.CO_Retencion_Venta_ID = ? " +
      "                 AND CO_RETENCION_VENTA.C_Invoice_ID = ? " +
      "        AND CO_RETENCION_VENTA.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND CO_RETENCION_VENTA.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, coNoEstablecimiento);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, coPtoEmision);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, noAutorizacion);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cDoctypeId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, documentno);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fechaEmision);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, tipoComprobanteVenta);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, noComprobanteVenta);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateacct);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, totalRetencion);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, processed);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isactive);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, posted);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, docactionre);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, processing);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, docstatus);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cDoctypetargetId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, coRetencionVentaId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cInvoiceId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, coRetencionVentaId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cInvoiceId);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public int insert(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        INSERT INTO CO_RETENCION_VENTA " +
      "        (AD_Org_ID, CO_No_Establecimiento, CO_Pto_Emision, NO_Autorizacion, C_Doctype_ID, DocumentNo, Fecha_Emision, Tipo_Comprobante_Venta, NO_Comprobante_Venta, Dateacct, Total_Retencion, Processed, Isactive, Posted, Docactionre, Processing, Docstatus, C_Doctypetarget_ID, CO_Retencion_Venta_ID, C_Invoice_ID, AD_Client_ID, created, createdby, updated, updatedBy)" +
      "        VALUES ((?), (?), (?), (?), (?), (?), TO_DATE(?), (?), (?), TO_DATE(?), TO_NUMBER(?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), now(), ?, now(), ?)";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, coNoEstablecimiento);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, coPtoEmision);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, noAutorizacion);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cDoctypeId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, documentno);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fechaEmision);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, tipoComprobanteVenta);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, noComprobanteVenta);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateacct);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, totalRetencion);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, processed);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isactive);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, posted);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, docactionre);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, processing);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, docstatus);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cDoctypetargetId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, coRetencionVentaId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cInvoiceId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, createdby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public static int delete(ConnectionProvider connectionProvider, String param1, String cInvoiceId, String adUserClient, String adOrgClient)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        DELETE FROM CO_RETENCION_VENTA" +
      "        WHERE CO_RETENCION_VENTA.CO_Retencion_Venta_ID = ? " +
      "                 AND CO_RETENCION_VENTA.C_Invoice_ID = ? " +
      "        AND CO_RETENCION_VENTA.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND CO_RETENCION_VENTA.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, param1);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cInvoiceId);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

/**
Select for relation
 */
  public static String selectOrg(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT AD_ORG_ID" +
      "          FROM CO_RETENCION_VENTA" +
      "         WHERE CO_RETENCION_VENTA.CO_Retencion_Venta_ID = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "ad_org_id");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public static String getCurrentDBTimestamp(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp" +
      "          FROM CO_RETENCION_VENTA" +
      "         WHERE CO_RETENCION_VENTA.CO_Retencion_Venta_ID = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "updated_time_stamp");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }
}
