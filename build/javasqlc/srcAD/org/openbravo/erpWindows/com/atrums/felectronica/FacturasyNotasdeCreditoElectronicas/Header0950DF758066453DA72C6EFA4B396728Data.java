//Sqlc generated V1.O00-1
package org.openbravo.erpWindows.com.atrums.felectronica.FacturasyNotasdeCreditoElectronicas;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import java.util.*;

/**
WAD Generated class
 */
class Header0950DF758066453DA72C6EFA4B396728Data implements FieldProvider {
static Logger log4j = Logger.getLogger(Header0950DF758066453DA72C6EFA4B396728Data.class);
  private String InitRecordNumber="0";
  public String created;
  public String createdbyr;
  public String updated;
  public String updatedTimeStamp;
  public String updatedby;
  public String updatedbyr;
  public String adOrgId;
  public String adOrgIdr;
  public String cDoctypetargetId;
  public String cDoctypetargetIdr;
  public String emCoNroEstab;
  public String emCoPuntoEmision;
  public String finPaymentPriorityId;
  public String documentno;
  public String emCoNroAutSri;
  public String emCoVencimientoAutSri;
  public String emAtecfeFechaSri;
  public String dateinvoiced;
  public String cBpartnerId;
  public String cBpartnerIdr;
  public String cBpartnerLocationId;
  public String cBpartnerLocationIdr;
  public String description;
  public String cPaymenttermId;
  public String finPaymentmethodId;
  public String docstatus;
  public String emAtecfeDocstatus;
  public String emAtecfeTotaldescuento;
  public String grandtotal;
  public String emAtecfeCInvoiceId;
  public String totallines;
  public String cCurrencyId;
  public String ispaid;
  public String adUserId;
  public String totalpaid;
  public String emAprmAddpayment;
  public String posted;
  public String emAprmProcessinvoice;
  public String emAtecfeDocaction;
  public String docaction;
  public String createfrom;
  public String copyfrom;
  public String emAtecfeImpXml;
  public String emAtecfeImpPdf;
  public String outstandingamt;
  public String dueamt;
  public String daystilldue;
  public String percentageoverdue;
  public String adOrgtrxId;
  public String finalsettlement;
  public String daysoutstanding;
  public String mPricelistId;
  public String salesrepId;
  public String salesrepIdr;
  public String cOrderId;
  public String poreference;
  public String dateacct;
  public String updatepaymentmonitor;
  public String taxdate;
  public String cActivityId;
  public String cChargeId;
  public String chargeamt;
  public String dateordered;
  public String cDoctypeId;
  public String calculatePromotions;
  public String cProjectId;
  public String cCostcenterId;
  public String aAssetId;
  public String cCampaignId;
  public String cCampaignIdr;
  public String user1Id;
  public String user2Id;
  public String emAtecfeMenobserrorSri;
  public String emAtecfeCodigoAcc;
  public String withholdingamount;
  public String cInvoiceId;
  public String isdiscountprinted;
  public String isprinted;
  public String lastcalculatedondate;
  public String isactive;
  public String processing;
  public String isselfservice;
  public String processed;
  public String istaxincluded;
  public String generateto;
  public String cWithholdingId;
  public String paymentrule;
  public String adClientId;
  public String dateprinted;
  public String issotrx;
  public String language;
  public String adUserClient;
  public String adOrgClient;
  public String createdby;
  public String trBgcolor;
  public String totalCount;
  public String dateTimeFormat;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("created"))
      return created;
    else if (fieldName.equalsIgnoreCase("createdbyr"))
      return createdbyr;
    else if (fieldName.equalsIgnoreCase("updated"))
      return updated;
    else if (fieldName.equalsIgnoreCase("updated_time_stamp") || fieldName.equals("updatedTimeStamp"))
      return updatedTimeStamp;
    else if (fieldName.equalsIgnoreCase("updatedby"))
      return updatedby;
    else if (fieldName.equalsIgnoreCase("updatedbyr"))
      return updatedbyr;
    else if (fieldName.equalsIgnoreCase("ad_org_id") || fieldName.equals("adOrgId"))
      return adOrgId;
    else if (fieldName.equalsIgnoreCase("ad_org_idr") || fieldName.equals("adOrgIdr"))
      return adOrgIdr;
    else if (fieldName.equalsIgnoreCase("c_doctypetarget_id") || fieldName.equals("cDoctypetargetId"))
      return cDoctypetargetId;
    else if (fieldName.equalsIgnoreCase("c_doctypetarget_idr") || fieldName.equals("cDoctypetargetIdr"))
      return cDoctypetargetIdr;
    else if (fieldName.equalsIgnoreCase("em_co_nro_estab") || fieldName.equals("emCoNroEstab"))
      return emCoNroEstab;
    else if (fieldName.equalsIgnoreCase("em_co_punto_emision") || fieldName.equals("emCoPuntoEmision"))
      return emCoPuntoEmision;
    else if (fieldName.equalsIgnoreCase("fin_payment_priority_id") || fieldName.equals("finPaymentPriorityId"))
      return finPaymentPriorityId;
    else if (fieldName.equalsIgnoreCase("documentno"))
      return documentno;
    else if (fieldName.equalsIgnoreCase("em_co_nro_aut_sri") || fieldName.equals("emCoNroAutSri"))
      return emCoNroAutSri;
    else if (fieldName.equalsIgnoreCase("em_co_vencimiento_aut_sri") || fieldName.equals("emCoVencimientoAutSri"))
      return emCoVencimientoAutSri;
    else if (fieldName.equalsIgnoreCase("em_atecfe_fecha_sri") || fieldName.equals("emAtecfeFechaSri"))
      return emAtecfeFechaSri;
    else if (fieldName.equalsIgnoreCase("dateinvoiced"))
      return dateinvoiced;
    else if (fieldName.equalsIgnoreCase("c_bpartner_id") || fieldName.equals("cBpartnerId"))
      return cBpartnerId;
    else if (fieldName.equalsIgnoreCase("c_bpartner_idr") || fieldName.equals("cBpartnerIdr"))
      return cBpartnerIdr;
    else if (fieldName.equalsIgnoreCase("c_bpartner_location_id") || fieldName.equals("cBpartnerLocationId"))
      return cBpartnerLocationId;
    else if (fieldName.equalsIgnoreCase("c_bpartner_location_idr") || fieldName.equals("cBpartnerLocationIdr"))
      return cBpartnerLocationIdr;
    else if (fieldName.equalsIgnoreCase("description"))
      return description;
    else if (fieldName.equalsIgnoreCase("c_paymentterm_id") || fieldName.equals("cPaymenttermId"))
      return cPaymenttermId;
    else if (fieldName.equalsIgnoreCase("fin_paymentmethod_id") || fieldName.equals("finPaymentmethodId"))
      return finPaymentmethodId;
    else if (fieldName.equalsIgnoreCase("docstatus"))
      return docstatus;
    else if (fieldName.equalsIgnoreCase("em_atecfe_docstatus") || fieldName.equals("emAtecfeDocstatus"))
      return emAtecfeDocstatus;
    else if (fieldName.equalsIgnoreCase("em_atecfe_totaldescuento") || fieldName.equals("emAtecfeTotaldescuento"))
      return emAtecfeTotaldescuento;
    else if (fieldName.equalsIgnoreCase("grandtotal"))
      return grandtotal;
    else if (fieldName.equalsIgnoreCase("em_atecfe_c_invoice_id") || fieldName.equals("emAtecfeCInvoiceId"))
      return emAtecfeCInvoiceId;
    else if (fieldName.equalsIgnoreCase("totallines"))
      return totallines;
    else if (fieldName.equalsIgnoreCase("c_currency_id") || fieldName.equals("cCurrencyId"))
      return cCurrencyId;
    else if (fieldName.equalsIgnoreCase("ispaid"))
      return ispaid;
    else if (fieldName.equalsIgnoreCase("ad_user_id") || fieldName.equals("adUserId"))
      return adUserId;
    else if (fieldName.equalsIgnoreCase("totalpaid"))
      return totalpaid;
    else if (fieldName.equalsIgnoreCase("em_aprm_addpayment") || fieldName.equals("emAprmAddpayment"))
      return emAprmAddpayment;
    else if (fieldName.equalsIgnoreCase("posted"))
      return posted;
    else if (fieldName.equalsIgnoreCase("em_aprm_processinvoice") || fieldName.equals("emAprmProcessinvoice"))
      return emAprmProcessinvoice;
    else if (fieldName.equalsIgnoreCase("em_atecfe_docaction") || fieldName.equals("emAtecfeDocaction"))
      return emAtecfeDocaction;
    else if (fieldName.equalsIgnoreCase("docaction"))
      return docaction;
    else if (fieldName.equalsIgnoreCase("createfrom"))
      return createfrom;
    else if (fieldName.equalsIgnoreCase("copyfrom"))
      return copyfrom;
    else if (fieldName.equalsIgnoreCase("em_atecfe_imp_xml") || fieldName.equals("emAtecfeImpXml"))
      return emAtecfeImpXml;
    else if (fieldName.equalsIgnoreCase("em_atecfe_imp_pdf") || fieldName.equals("emAtecfeImpPdf"))
      return emAtecfeImpPdf;
    else if (fieldName.equalsIgnoreCase("outstandingamt"))
      return outstandingamt;
    else if (fieldName.equalsIgnoreCase("dueamt"))
      return dueamt;
    else if (fieldName.equalsIgnoreCase("daystilldue"))
      return daystilldue;
    else if (fieldName.equalsIgnoreCase("percentageoverdue"))
      return percentageoverdue;
    else if (fieldName.equalsIgnoreCase("ad_orgtrx_id") || fieldName.equals("adOrgtrxId"))
      return adOrgtrxId;
    else if (fieldName.equalsIgnoreCase("finalsettlement"))
      return finalsettlement;
    else if (fieldName.equalsIgnoreCase("daysoutstanding"))
      return daysoutstanding;
    else if (fieldName.equalsIgnoreCase("m_pricelist_id") || fieldName.equals("mPricelistId"))
      return mPricelistId;
    else if (fieldName.equalsIgnoreCase("salesrep_id") || fieldName.equals("salesrepId"))
      return salesrepId;
    else if (fieldName.equalsIgnoreCase("salesrep_idr") || fieldName.equals("salesrepIdr"))
      return salesrepIdr;
    else if (fieldName.equalsIgnoreCase("c_order_id") || fieldName.equals("cOrderId"))
      return cOrderId;
    else if (fieldName.equalsIgnoreCase("poreference"))
      return poreference;
    else if (fieldName.equalsIgnoreCase("dateacct"))
      return dateacct;
    else if (fieldName.equalsIgnoreCase("updatepaymentmonitor"))
      return updatepaymentmonitor;
    else if (fieldName.equalsIgnoreCase("taxdate"))
      return taxdate;
    else if (fieldName.equalsIgnoreCase("c_activity_id") || fieldName.equals("cActivityId"))
      return cActivityId;
    else if (fieldName.equalsIgnoreCase("c_charge_id") || fieldName.equals("cChargeId"))
      return cChargeId;
    else if (fieldName.equalsIgnoreCase("chargeamt"))
      return chargeamt;
    else if (fieldName.equalsIgnoreCase("dateordered"))
      return dateordered;
    else if (fieldName.equalsIgnoreCase("c_doctype_id") || fieldName.equals("cDoctypeId"))
      return cDoctypeId;
    else if (fieldName.equalsIgnoreCase("calculate_promotions") || fieldName.equals("calculatePromotions"))
      return calculatePromotions;
    else if (fieldName.equalsIgnoreCase("c_project_id") || fieldName.equals("cProjectId"))
      return cProjectId;
    else if (fieldName.equalsIgnoreCase("c_costcenter_id") || fieldName.equals("cCostcenterId"))
      return cCostcenterId;
    else if (fieldName.equalsIgnoreCase("a_asset_id") || fieldName.equals("aAssetId"))
      return aAssetId;
    else if (fieldName.equalsIgnoreCase("c_campaign_id") || fieldName.equals("cCampaignId"))
      return cCampaignId;
    else if (fieldName.equalsIgnoreCase("c_campaign_idr") || fieldName.equals("cCampaignIdr"))
      return cCampaignIdr;
    else if (fieldName.equalsIgnoreCase("user1_id") || fieldName.equals("user1Id"))
      return user1Id;
    else if (fieldName.equalsIgnoreCase("user2_id") || fieldName.equals("user2Id"))
      return user2Id;
    else if (fieldName.equalsIgnoreCase("em_atecfe_menobserror_sri") || fieldName.equals("emAtecfeMenobserrorSri"))
      return emAtecfeMenobserrorSri;
    else if (fieldName.equalsIgnoreCase("em_atecfe_codigo_acc") || fieldName.equals("emAtecfeCodigoAcc"))
      return emAtecfeCodigoAcc;
    else if (fieldName.equalsIgnoreCase("withholdingamount"))
      return withholdingamount;
    else if (fieldName.equalsIgnoreCase("c_invoice_id") || fieldName.equals("cInvoiceId"))
      return cInvoiceId;
    else if (fieldName.equalsIgnoreCase("isdiscountprinted"))
      return isdiscountprinted;
    else if (fieldName.equalsIgnoreCase("isprinted"))
      return isprinted;
    else if (fieldName.equalsIgnoreCase("lastcalculatedondate"))
      return lastcalculatedondate;
    else if (fieldName.equalsIgnoreCase("isactive"))
      return isactive;
    else if (fieldName.equalsIgnoreCase("processing"))
      return processing;
    else if (fieldName.equalsIgnoreCase("isselfservice"))
      return isselfservice;
    else if (fieldName.equalsIgnoreCase("processed"))
      return processed;
    else if (fieldName.equalsIgnoreCase("istaxincluded"))
      return istaxincluded;
    else if (fieldName.equalsIgnoreCase("generateto"))
      return generateto;
    else if (fieldName.equalsIgnoreCase("c_withholding_id") || fieldName.equals("cWithholdingId"))
      return cWithholdingId;
    else if (fieldName.equalsIgnoreCase("paymentrule"))
      return paymentrule;
    else if (fieldName.equalsIgnoreCase("ad_client_id") || fieldName.equals("adClientId"))
      return adClientId;
    else if (fieldName.equalsIgnoreCase("dateprinted"))
      return dateprinted;
    else if (fieldName.equalsIgnoreCase("issotrx"))
      return issotrx;
    else if (fieldName.equalsIgnoreCase("language"))
      return language;
    else if (fieldName.equals("adUserClient"))
      return adUserClient;
    else if (fieldName.equals("adOrgClient"))
      return adOrgClient;
    else if (fieldName.equals("createdby"))
      return createdby;
    else if (fieldName.equals("trBgcolor"))
      return trBgcolor;
    else if (fieldName.equals("totalCount"))
      return totalCount;
    else if (fieldName.equals("dateTimeFormat"))
      return dateTimeFormat;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

/**
Select for edit
 */
  public static Header0950DF758066453DA72C6EFA4B396728Data[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String key, String adUserClient, String adOrgClient)    throws ServletException {
    return selectEdit(connectionProvider, dateTimeFormat, paramLanguage, key, adUserClient, adOrgClient, 0, 0);
  }

/**
Select for edit
 */
  public static Header0950DF758066453DA72C6EFA4B396728Data[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String key, String adUserClient, String adOrgClient, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(C_Invoice.Created, ?) as created, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = C_Invoice.CreatedBy) as CreatedByR, " +
      "        to_char(C_Invoice.Updated, ?) as updated, " +
      "        to_char(C_Invoice.Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp,  " +
      "        C_Invoice.UpdatedBy, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = C_Invoice.UpdatedBy) as UpdatedByR," +
      "        C_Invoice.AD_Org_ID, " +
      "(CASE WHEN C_Invoice.AD_Org_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table1.Name), ''))),'') ) END) AS AD_Org_IDR, " +
      "C_Invoice.C_DocTypeTarget_ID, " +
      "(CASE WHEN C_Invoice.C_DocTypeTarget_ID IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR((CASE WHEN tableTRL2.Name IS NULL THEN TO_CHAR(table2.Name) ELSE TO_CHAR(tableTRL2.Name) END)), ''))),'') ) END) AS C_DocTypeTarget_IDR, " +
      "C_Invoice.EM_Co_Nro_Estab, " +
      "C_Invoice.EM_Co_Punto_Emision, " +
      "C_Invoice.FIN_Payment_Priority_ID, " +
      "C_Invoice.DocumentNo, " +
      "C_Invoice.EM_Co_Nro_Aut_Sri, " +
      "C_Invoice.EM_Co_Vencimiento_Aut_Sri, " +
      "TO_CHAR(C_Invoice.EM_Atecfe_Fecha_Sri, ?) AS EM_Atecfe_Fecha_Sri, " +
      "C_Invoice.DateInvoiced, " +
      "C_Invoice.C_BPartner_ID, " +
      "(CASE WHEN C_Invoice.C_BPartner_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table4.Name), ''))),'') ) END) AS C_BPartner_IDR, " +
      "C_Invoice.C_BPartner_Location_ID, " +
      "(CASE WHEN C_Invoice.C_BPartner_Location_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table5.Name), ''))),'') ) END) AS C_BPartner_Location_IDR, " +
      "C_Invoice.Description, " +
      "C_Invoice.C_PaymentTerm_ID, " +
      "C_Invoice.FIN_Paymentmethod_ID, " +
      "C_Invoice.DocStatus, " +
      "C_Invoice.EM_Atecfe_Docstatus, " +
      "C_Invoice.EM_Atecfe_Totaldescuento, " +
      "C_Invoice.GrandTotal, " +
      "C_Invoice.EM_Atecfe_C_Invoice_ID, " +
      "C_Invoice.TotalLines, " +
      "C_Invoice.C_Currency_ID, " +
      "COALESCE(C_Invoice.Ispaid, 'N') AS Ispaid, " +
      "C_Invoice.AD_User_ID, " +
      "C_Invoice.Totalpaid, " +
      "C_Invoice.EM_APRM_Addpayment, " +
      "C_Invoice.Posted, " +
      "C_Invoice.EM_APRM_Processinvoice, " +
      "C_Invoice.EM_Atecfe_Docaction, " +
      "C_Invoice.DocAction, " +
      "C_Invoice.CreateFrom, " +
      "C_Invoice.CopyFrom, " +
      "C_Invoice.EM_Atecfe_Imp_Xml, " +
      "C_Invoice.EM_Atecfe_Imp_Pdf, " +
      "C_Invoice.OutstandingAmt, " +
      "C_Invoice.DueAmt, " +
      "C_Invoice.DaysTillDue, " +
      "C_Invoice.Percentageoverdue, " +
      "C_Invoice.AD_OrgTrx_ID, " +
      "C_Invoice.Finalsettlement, " +
      "C_Invoice.Daysoutstanding, " +
      "C_Invoice.M_PriceList_ID, " +
      "C_Invoice.SalesRep_ID, " +
      "(CASE WHEN C_Invoice.SalesRep_ID IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table6.Name), ''))),'') ) END) AS SalesRep_IDR, " +
      "C_Invoice.C_Order_ID, " +
      "C_Invoice.POReference, " +
      "C_Invoice.DateAcct, " +
      "C_Invoice.UpdatePaymentMonitor, " +
      "C_Invoice.Taxdate, " +
      "C_Invoice.C_Activity_ID, " +
      "C_Invoice.C_Charge_ID, " +
      "C_Invoice.ChargeAmt, " +
      "C_Invoice.DateOrdered, " +
      "C_Invoice.C_DocType_ID, " +
      "C_Invoice.Calculate_Promotions, " +
      "C_Invoice.C_Project_ID, " +
      "C_Invoice.C_Costcenter_ID, " +
      "C_Invoice.A_Asset_ID, " +
      "C_Invoice.C_Campaign_ID, " +
      "(CASE WHEN C_Invoice.C_Campaign_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table7.Name), ''))),'') ) END) AS C_Campaign_IDR, " +
      "C_Invoice.User1_ID, " +
      "C_Invoice.User2_ID, " +
      "C_Invoice.Em_Atecfe_Menobserror_Sri, " +
      "C_Invoice.EM_Atecfe_Codigo_Acc, " +
      "C_Invoice.Withholdingamount, " +
      "C_Invoice.C_Invoice_ID, " +
      "COALESCE(C_Invoice.IsDiscountPrinted, 'N') AS IsDiscountPrinted, " +
      "COALESCE(C_Invoice.IsPrinted, 'N') AS IsPrinted, " +
      "C_Invoice.LastCalculatedOnDate, " +
      "COALESCE(C_Invoice.IsActive, 'N') AS IsActive, " +
      "C_Invoice.Processing, " +
      "COALESCE(C_Invoice.IsSelfService, 'N') AS IsSelfService, " +
      "COALESCE(C_Invoice.Processed, 'N') AS Processed, " +
      "COALESCE(C_Invoice.IsTaxIncluded, 'N') AS IsTaxIncluded, " +
      "C_Invoice.GenerateTo, " +
      "C_Invoice.C_Withholding_ID, " +
      "C_Invoice.PaymentRule, " +
      "C_Invoice.AD_Client_ID, " +
      "C_Invoice.DatePrinted, " +
      "COALESCE(C_Invoice.IsSOTrx, 'N') AS IsSOTrx, " +
      "        ? AS LANGUAGE " +
      "        FROM C_Invoice left join (select AD_Org_ID, Name from AD_Org) table1 on (C_Invoice.AD_Org_ID = table1.AD_Org_ID) left join (select C_DocType_ID, Name from C_DocType) table2 on (C_Invoice.C_DocTypeTarget_ID =  table2.C_DocType_ID) left join (select C_DocType_ID,AD_Language, Name from C_DocType_TRL) tableTRL2 on (table2.C_DocType_ID = tableTRL2.C_DocType_ID and tableTRL2.AD_Language = ?)  left join (select C_BPartner_ID, Name from C_BPartner) table4 on (C_Invoice.C_BPartner_ID = table4.C_BPartner_ID) left join (select C_BPartner_Location_ID, Name from C_BPartner_Location) table5 on (C_Invoice.C_BPartner_Location_ID = table5.C_BPartner_Location_ID) left join (select AD_User_ID, Name from AD_User) table6 on (C_Invoice.SalesRep_ID =  table6.AD_User_ID) left join (select C_Campaign_ID, Name from C_Campaign) table7 on (C_Invoice.C_Campaign_ID = table7.C_Campaign_ID)" +
      "        WHERE 2=2 " +
      " AND C_Invoice.IsSOTrx='Y'" +
      "        AND 1=1 " +
      "        AND C_Invoice.C_Invoice_ID = ? " +
      "        AND C_Invoice.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "           AND C_Invoice.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, key);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        Header0950DF758066453DA72C6EFA4B396728Data objectHeader0950DF758066453DA72C6EFA4B396728Data = new Header0950DF758066453DA72C6EFA4B396728Data();
        objectHeader0950DF758066453DA72C6EFA4B396728Data.created = UtilSql.getValue(result, "created");
        objectHeader0950DF758066453DA72C6EFA4B396728Data.createdbyr = UtilSql.getValue(result, "createdbyr");
        objectHeader0950DF758066453DA72C6EFA4B396728Data.updated = UtilSql.getValue(result, "updated");
        objectHeader0950DF758066453DA72C6EFA4B396728Data.updatedTimeStamp = UtilSql.getValue(result, "updated_time_stamp");
        objectHeader0950DF758066453DA72C6EFA4B396728Data.updatedby = UtilSql.getValue(result, "updatedby");
        objectHeader0950DF758066453DA72C6EFA4B396728Data.updatedbyr = UtilSql.getValue(result, "updatedbyr");
        objectHeader0950DF758066453DA72C6EFA4B396728Data.adOrgId = UtilSql.getValue(result, "ad_org_id");
        objectHeader0950DF758066453DA72C6EFA4B396728Data.adOrgIdr = UtilSql.getValue(result, "ad_org_idr");
        objectHeader0950DF758066453DA72C6EFA4B396728Data.cDoctypetargetId = UtilSql.getValue(result, "c_doctypetarget_id");
        objectHeader0950DF758066453DA72C6EFA4B396728Data.cDoctypetargetIdr = UtilSql.getValue(result, "c_doctypetarget_idr");
        objectHeader0950DF758066453DA72C6EFA4B396728Data.emCoNroEstab = UtilSql.getValue(result, "em_co_nro_estab");
        objectHeader0950DF758066453DA72C6EFA4B396728Data.emCoPuntoEmision = UtilSql.getValue(result, "em_co_punto_emision");
        objectHeader0950DF758066453DA72C6EFA4B396728Data.finPaymentPriorityId = UtilSql.getValue(result, "fin_payment_priority_id");
        objectHeader0950DF758066453DA72C6EFA4B396728Data.documentno = UtilSql.getValue(result, "documentno");
        objectHeader0950DF758066453DA72C6EFA4B396728Data.emCoNroAutSri = UtilSql.getValue(result, "em_co_nro_aut_sri");
        objectHeader0950DF758066453DA72C6EFA4B396728Data.emCoVencimientoAutSri = UtilSql.getDateValue(result, "em_co_vencimiento_aut_sri", "dd-MM-yyyy");
        objectHeader0950DF758066453DA72C6EFA4B396728Data.emAtecfeFechaSri = UtilSql.getValue(result, "em_atecfe_fecha_sri");
        objectHeader0950DF758066453DA72C6EFA4B396728Data.dateinvoiced = UtilSql.getDateValue(result, "dateinvoiced", "dd-MM-yyyy");
        objectHeader0950DF758066453DA72C6EFA4B396728Data.cBpartnerId = UtilSql.getValue(result, "c_bpartner_id");
        objectHeader0950DF758066453DA72C6EFA4B396728Data.cBpartnerIdr = UtilSql.getValue(result, "c_bpartner_idr");
        objectHeader0950DF758066453DA72C6EFA4B396728Data.cBpartnerLocationId = UtilSql.getValue(result, "c_bpartner_location_id");
        objectHeader0950DF758066453DA72C6EFA4B396728Data.cBpartnerLocationIdr = UtilSql.getValue(result, "c_bpartner_location_idr");
        objectHeader0950DF758066453DA72C6EFA4B396728Data.description = UtilSql.getValue(result, "description");
        objectHeader0950DF758066453DA72C6EFA4B396728Data.cPaymenttermId = UtilSql.getValue(result, "c_paymentterm_id");
        objectHeader0950DF758066453DA72C6EFA4B396728Data.finPaymentmethodId = UtilSql.getValue(result, "fin_paymentmethod_id");
        objectHeader0950DF758066453DA72C6EFA4B396728Data.docstatus = UtilSql.getValue(result, "docstatus");
        objectHeader0950DF758066453DA72C6EFA4B396728Data.emAtecfeDocstatus = UtilSql.getValue(result, "em_atecfe_docstatus");
        objectHeader0950DF758066453DA72C6EFA4B396728Data.emAtecfeTotaldescuento = UtilSql.getValue(result, "em_atecfe_totaldescuento");
        objectHeader0950DF758066453DA72C6EFA4B396728Data.grandtotal = UtilSql.getValue(result, "grandtotal");
        objectHeader0950DF758066453DA72C6EFA4B396728Data.emAtecfeCInvoiceId = UtilSql.getValue(result, "em_atecfe_c_invoice_id");
        objectHeader0950DF758066453DA72C6EFA4B396728Data.totallines = UtilSql.getValue(result, "totallines");
        objectHeader0950DF758066453DA72C6EFA4B396728Data.cCurrencyId = UtilSql.getValue(result, "c_currency_id");
        objectHeader0950DF758066453DA72C6EFA4B396728Data.ispaid = UtilSql.getValue(result, "ispaid");
        objectHeader0950DF758066453DA72C6EFA4B396728Data.adUserId = UtilSql.getValue(result, "ad_user_id");
        objectHeader0950DF758066453DA72C6EFA4B396728Data.totalpaid = UtilSql.getValue(result, "totalpaid");
        objectHeader0950DF758066453DA72C6EFA4B396728Data.emAprmAddpayment = UtilSql.getValue(result, "em_aprm_addpayment");
        objectHeader0950DF758066453DA72C6EFA4B396728Data.posted = UtilSql.getValue(result, "posted");
        objectHeader0950DF758066453DA72C6EFA4B396728Data.emAprmProcessinvoice = UtilSql.getValue(result, "em_aprm_processinvoice");
        objectHeader0950DF758066453DA72C6EFA4B396728Data.emAtecfeDocaction = UtilSql.getValue(result, "em_atecfe_docaction");
        objectHeader0950DF758066453DA72C6EFA4B396728Data.docaction = UtilSql.getValue(result, "docaction");
        objectHeader0950DF758066453DA72C6EFA4B396728Data.createfrom = UtilSql.getValue(result, "createfrom");
        objectHeader0950DF758066453DA72C6EFA4B396728Data.copyfrom = UtilSql.getValue(result, "copyfrom");
        objectHeader0950DF758066453DA72C6EFA4B396728Data.emAtecfeImpXml = UtilSql.getValue(result, "em_atecfe_imp_xml");
        objectHeader0950DF758066453DA72C6EFA4B396728Data.emAtecfeImpPdf = UtilSql.getValue(result, "em_atecfe_imp_pdf");
        objectHeader0950DF758066453DA72C6EFA4B396728Data.outstandingamt = UtilSql.getValue(result, "outstandingamt");
        objectHeader0950DF758066453DA72C6EFA4B396728Data.dueamt = UtilSql.getValue(result, "dueamt");
        objectHeader0950DF758066453DA72C6EFA4B396728Data.daystilldue = UtilSql.getValue(result, "daystilldue");
        objectHeader0950DF758066453DA72C6EFA4B396728Data.percentageoverdue = UtilSql.getValue(result, "percentageoverdue");
        objectHeader0950DF758066453DA72C6EFA4B396728Data.adOrgtrxId = UtilSql.getValue(result, "ad_orgtrx_id");
        objectHeader0950DF758066453DA72C6EFA4B396728Data.finalsettlement = UtilSql.getDateValue(result, "finalsettlement", "dd-MM-yyyy");
        objectHeader0950DF758066453DA72C6EFA4B396728Data.daysoutstanding = UtilSql.getValue(result, "daysoutstanding");
        objectHeader0950DF758066453DA72C6EFA4B396728Data.mPricelistId = UtilSql.getValue(result, "m_pricelist_id");
        objectHeader0950DF758066453DA72C6EFA4B396728Data.salesrepId = UtilSql.getValue(result, "salesrep_id");
        objectHeader0950DF758066453DA72C6EFA4B396728Data.salesrepIdr = UtilSql.getValue(result, "salesrep_idr");
        objectHeader0950DF758066453DA72C6EFA4B396728Data.cOrderId = UtilSql.getValue(result, "c_order_id");
        objectHeader0950DF758066453DA72C6EFA4B396728Data.poreference = UtilSql.getValue(result, "poreference");
        objectHeader0950DF758066453DA72C6EFA4B396728Data.dateacct = UtilSql.getDateValue(result, "dateacct", "dd-MM-yyyy");
        objectHeader0950DF758066453DA72C6EFA4B396728Data.updatepaymentmonitor = UtilSql.getValue(result, "updatepaymentmonitor");
        objectHeader0950DF758066453DA72C6EFA4B396728Data.taxdate = UtilSql.getDateValue(result, "taxdate", "dd-MM-yyyy");
        objectHeader0950DF758066453DA72C6EFA4B396728Data.cActivityId = UtilSql.getValue(result, "c_activity_id");
        objectHeader0950DF758066453DA72C6EFA4B396728Data.cChargeId = UtilSql.getValue(result, "c_charge_id");
        objectHeader0950DF758066453DA72C6EFA4B396728Data.chargeamt = UtilSql.getValue(result, "chargeamt");
        objectHeader0950DF758066453DA72C6EFA4B396728Data.dateordered = UtilSql.getDateValue(result, "dateordered", "dd-MM-yyyy");
        objectHeader0950DF758066453DA72C6EFA4B396728Data.cDoctypeId = UtilSql.getValue(result, "c_doctype_id");
        objectHeader0950DF758066453DA72C6EFA4B396728Data.calculatePromotions = UtilSql.getValue(result, "calculate_promotions");
        objectHeader0950DF758066453DA72C6EFA4B396728Data.cProjectId = UtilSql.getValue(result, "c_project_id");
        objectHeader0950DF758066453DA72C6EFA4B396728Data.cCostcenterId = UtilSql.getValue(result, "c_costcenter_id");
        objectHeader0950DF758066453DA72C6EFA4B396728Data.aAssetId = UtilSql.getValue(result, "a_asset_id");
        objectHeader0950DF758066453DA72C6EFA4B396728Data.cCampaignId = UtilSql.getValue(result, "c_campaign_id");
        objectHeader0950DF758066453DA72C6EFA4B396728Data.cCampaignIdr = UtilSql.getValue(result, "c_campaign_idr");
        objectHeader0950DF758066453DA72C6EFA4B396728Data.user1Id = UtilSql.getValue(result, "user1_id");
        objectHeader0950DF758066453DA72C6EFA4B396728Data.user2Id = UtilSql.getValue(result, "user2_id");
        objectHeader0950DF758066453DA72C6EFA4B396728Data.emAtecfeMenobserrorSri = UtilSql.getValue(result, "em_atecfe_menobserror_sri");
        objectHeader0950DF758066453DA72C6EFA4B396728Data.emAtecfeCodigoAcc = UtilSql.getValue(result, "em_atecfe_codigo_acc");
        objectHeader0950DF758066453DA72C6EFA4B396728Data.withholdingamount = UtilSql.getValue(result, "withholdingamount");
        objectHeader0950DF758066453DA72C6EFA4B396728Data.cInvoiceId = UtilSql.getValue(result, "c_invoice_id");
        objectHeader0950DF758066453DA72C6EFA4B396728Data.isdiscountprinted = UtilSql.getValue(result, "isdiscountprinted");
        objectHeader0950DF758066453DA72C6EFA4B396728Data.isprinted = UtilSql.getValue(result, "isprinted");
        objectHeader0950DF758066453DA72C6EFA4B396728Data.lastcalculatedondate = UtilSql.getDateValue(result, "lastcalculatedondate", "dd-MM-yyyy");
        objectHeader0950DF758066453DA72C6EFA4B396728Data.isactive = UtilSql.getValue(result, "isactive");
        objectHeader0950DF758066453DA72C6EFA4B396728Data.processing = UtilSql.getValue(result, "processing");
        objectHeader0950DF758066453DA72C6EFA4B396728Data.isselfservice = UtilSql.getValue(result, "isselfservice");
        objectHeader0950DF758066453DA72C6EFA4B396728Data.processed = UtilSql.getValue(result, "processed");
        objectHeader0950DF758066453DA72C6EFA4B396728Data.istaxincluded = UtilSql.getValue(result, "istaxincluded");
        objectHeader0950DF758066453DA72C6EFA4B396728Data.generateto = UtilSql.getValue(result, "generateto");
        objectHeader0950DF758066453DA72C6EFA4B396728Data.cWithholdingId = UtilSql.getValue(result, "c_withholding_id");
        objectHeader0950DF758066453DA72C6EFA4B396728Data.paymentrule = UtilSql.getValue(result, "paymentrule");
        objectHeader0950DF758066453DA72C6EFA4B396728Data.adClientId = UtilSql.getValue(result, "ad_client_id");
        objectHeader0950DF758066453DA72C6EFA4B396728Data.dateprinted = UtilSql.getDateValue(result, "dateprinted", "dd-MM-yyyy");
        objectHeader0950DF758066453DA72C6EFA4B396728Data.issotrx = UtilSql.getValue(result, "issotrx");
        objectHeader0950DF758066453DA72C6EFA4B396728Data.language = UtilSql.getValue(result, "language");
        objectHeader0950DF758066453DA72C6EFA4B396728Data.adUserClient = "";
        objectHeader0950DF758066453DA72C6EFA4B396728Data.adOrgClient = "";
        objectHeader0950DF758066453DA72C6EFA4B396728Data.createdby = "";
        objectHeader0950DF758066453DA72C6EFA4B396728Data.trBgcolor = "";
        objectHeader0950DF758066453DA72C6EFA4B396728Data.totalCount = "";
        objectHeader0950DF758066453DA72C6EFA4B396728Data.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectHeader0950DF758066453DA72C6EFA4B396728Data);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    Header0950DF758066453DA72C6EFA4B396728Data objectHeader0950DF758066453DA72C6EFA4B396728Data[] = new Header0950DF758066453DA72C6EFA4B396728Data[vector.size()];
    vector.copyInto(objectHeader0950DF758066453DA72C6EFA4B396728Data);
    return(objectHeader0950DF758066453DA72C6EFA4B396728Data);
  }

/**
Create a registry
 */
  public static Header0950DF758066453DA72C6EFA4B396728Data[] set(String finPaymentPriorityId, String lastcalculatedondate, String cWithholdingId, String withholdingamount, String taxdate, String emAtecfeDocaction, String emAtecfeMenobserrorSri, String emCoNroEstab, String daystilldue, String cInvoiceId, String adClientId, String adOrgId, String isactive, String created, String createdby, String createdbyr, String updatedby, String updatedbyr, String documentno, String cDoctypeId, String docstatus, String docaction, String processing, String processed, String cBpartnerId, String cBpartnerIdr, String cPaymenttermId, String cBpartnerLocationId, String cCurrencyId, String totallines, String grandtotal, String dateacct, String cCampaignId, String cProjectId, String cActivityId, String salesrepId, String adUserId, String issotrx, String cDoctypetargetId, String description, String dateinvoiced, String dateprinted, String poreference, String cChargeId, String chargeamt, String mPricelistId, String paymentrule, String cOrderId, String dateordered, String isdiscountprinted, String isprinted, String emAtecfeDocstatus, String istaxincluded, String posted, String generateto, String createfrom, String emCoPuntoEmision, String emAprmAddpayment, String emAtecfeTotaldescuento, String finPaymentmethodId, String emAtecfeImpXml, String copyfrom, String isselfservice, String emAprmProcessinvoice, String cCostcenterId, String emAtecfeCodigoAcc, String adOrgtrxId, String user1Id, String user2Id, String finalsettlement, String daysoutstanding, String percentageoverdue, String totalpaid, String emAtecfeImpPdf, String outstandingamt, String dueamt, String emCoNroAutSri, String ispaid, String emAtecfeFechaSri, String updatepaymentmonitor, String aAssetId, String calculatePromotions, String emAtecfeCInvoiceId, String emCoVencimientoAutSri)    throws ServletException {
    Header0950DF758066453DA72C6EFA4B396728Data objectHeader0950DF758066453DA72C6EFA4B396728Data[] = new Header0950DF758066453DA72C6EFA4B396728Data[1];
    objectHeader0950DF758066453DA72C6EFA4B396728Data[0] = new Header0950DF758066453DA72C6EFA4B396728Data();
    objectHeader0950DF758066453DA72C6EFA4B396728Data[0].created = created;
    objectHeader0950DF758066453DA72C6EFA4B396728Data[0].createdbyr = createdbyr;
    objectHeader0950DF758066453DA72C6EFA4B396728Data[0].updated = "";
    objectHeader0950DF758066453DA72C6EFA4B396728Data[0].updatedTimeStamp = "";
    objectHeader0950DF758066453DA72C6EFA4B396728Data[0].updatedby = updatedby;
    objectHeader0950DF758066453DA72C6EFA4B396728Data[0].updatedbyr = updatedbyr;
    objectHeader0950DF758066453DA72C6EFA4B396728Data[0].adOrgId = adOrgId;
    objectHeader0950DF758066453DA72C6EFA4B396728Data[0].adOrgIdr = "";
    objectHeader0950DF758066453DA72C6EFA4B396728Data[0].cDoctypetargetId = cDoctypetargetId;
    objectHeader0950DF758066453DA72C6EFA4B396728Data[0].cDoctypetargetIdr = "";
    objectHeader0950DF758066453DA72C6EFA4B396728Data[0].emCoNroEstab = emCoNroEstab;
    objectHeader0950DF758066453DA72C6EFA4B396728Data[0].emCoPuntoEmision = emCoPuntoEmision;
    objectHeader0950DF758066453DA72C6EFA4B396728Data[0].finPaymentPriorityId = finPaymentPriorityId;
    objectHeader0950DF758066453DA72C6EFA4B396728Data[0].documentno = documentno;
    objectHeader0950DF758066453DA72C6EFA4B396728Data[0].emCoNroAutSri = emCoNroAutSri;
    objectHeader0950DF758066453DA72C6EFA4B396728Data[0].emCoVencimientoAutSri = emCoVencimientoAutSri;
    objectHeader0950DF758066453DA72C6EFA4B396728Data[0].emAtecfeFechaSri = emAtecfeFechaSri;
    objectHeader0950DF758066453DA72C6EFA4B396728Data[0].dateinvoiced = dateinvoiced;
    objectHeader0950DF758066453DA72C6EFA4B396728Data[0].cBpartnerId = cBpartnerId;
    objectHeader0950DF758066453DA72C6EFA4B396728Data[0].cBpartnerIdr = cBpartnerIdr;
    objectHeader0950DF758066453DA72C6EFA4B396728Data[0].cBpartnerLocationId = cBpartnerLocationId;
    objectHeader0950DF758066453DA72C6EFA4B396728Data[0].cBpartnerLocationIdr = "";
    objectHeader0950DF758066453DA72C6EFA4B396728Data[0].description = description;
    objectHeader0950DF758066453DA72C6EFA4B396728Data[0].cPaymenttermId = cPaymenttermId;
    objectHeader0950DF758066453DA72C6EFA4B396728Data[0].finPaymentmethodId = finPaymentmethodId;
    objectHeader0950DF758066453DA72C6EFA4B396728Data[0].docstatus = docstatus;
    objectHeader0950DF758066453DA72C6EFA4B396728Data[0].emAtecfeDocstatus = emAtecfeDocstatus;
    objectHeader0950DF758066453DA72C6EFA4B396728Data[0].emAtecfeTotaldescuento = emAtecfeTotaldescuento;
    objectHeader0950DF758066453DA72C6EFA4B396728Data[0].grandtotal = grandtotal;
    objectHeader0950DF758066453DA72C6EFA4B396728Data[0].emAtecfeCInvoiceId = emAtecfeCInvoiceId;
    objectHeader0950DF758066453DA72C6EFA4B396728Data[0].totallines = totallines;
    objectHeader0950DF758066453DA72C6EFA4B396728Data[0].cCurrencyId = cCurrencyId;
    objectHeader0950DF758066453DA72C6EFA4B396728Data[0].ispaid = ispaid;
    objectHeader0950DF758066453DA72C6EFA4B396728Data[0].adUserId = adUserId;
    objectHeader0950DF758066453DA72C6EFA4B396728Data[0].totalpaid = totalpaid;
    objectHeader0950DF758066453DA72C6EFA4B396728Data[0].emAprmAddpayment = emAprmAddpayment;
    objectHeader0950DF758066453DA72C6EFA4B396728Data[0].posted = posted;
    objectHeader0950DF758066453DA72C6EFA4B396728Data[0].emAprmProcessinvoice = emAprmProcessinvoice;
    objectHeader0950DF758066453DA72C6EFA4B396728Data[0].emAtecfeDocaction = emAtecfeDocaction;
    objectHeader0950DF758066453DA72C6EFA4B396728Data[0].docaction = docaction;
    objectHeader0950DF758066453DA72C6EFA4B396728Data[0].createfrom = createfrom;
    objectHeader0950DF758066453DA72C6EFA4B396728Data[0].copyfrom = copyfrom;
    objectHeader0950DF758066453DA72C6EFA4B396728Data[0].emAtecfeImpXml = emAtecfeImpXml;
    objectHeader0950DF758066453DA72C6EFA4B396728Data[0].emAtecfeImpPdf = emAtecfeImpPdf;
    objectHeader0950DF758066453DA72C6EFA4B396728Data[0].outstandingamt = outstandingamt;
    objectHeader0950DF758066453DA72C6EFA4B396728Data[0].dueamt = dueamt;
    objectHeader0950DF758066453DA72C6EFA4B396728Data[0].daystilldue = daystilldue;
    objectHeader0950DF758066453DA72C6EFA4B396728Data[0].percentageoverdue = percentageoverdue;
    objectHeader0950DF758066453DA72C6EFA4B396728Data[0].adOrgtrxId = adOrgtrxId;
    objectHeader0950DF758066453DA72C6EFA4B396728Data[0].finalsettlement = finalsettlement;
    objectHeader0950DF758066453DA72C6EFA4B396728Data[0].daysoutstanding = daysoutstanding;
    objectHeader0950DF758066453DA72C6EFA4B396728Data[0].mPricelistId = mPricelistId;
    objectHeader0950DF758066453DA72C6EFA4B396728Data[0].salesrepId = salesrepId;
    objectHeader0950DF758066453DA72C6EFA4B396728Data[0].salesrepIdr = "";
    objectHeader0950DF758066453DA72C6EFA4B396728Data[0].cOrderId = cOrderId;
    objectHeader0950DF758066453DA72C6EFA4B396728Data[0].poreference = poreference;
    objectHeader0950DF758066453DA72C6EFA4B396728Data[0].dateacct = dateacct;
    objectHeader0950DF758066453DA72C6EFA4B396728Data[0].updatepaymentmonitor = updatepaymentmonitor;
    objectHeader0950DF758066453DA72C6EFA4B396728Data[0].taxdate = taxdate;
    objectHeader0950DF758066453DA72C6EFA4B396728Data[0].cActivityId = cActivityId;
    objectHeader0950DF758066453DA72C6EFA4B396728Data[0].cChargeId = cChargeId;
    objectHeader0950DF758066453DA72C6EFA4B396728Data[0].chargeamt = chargeamt;
    objectHeader0950DF758066453DA72C6EFA4B396728Data[0].dateordered = dateordered;
    objectHeader0950DF758066453DA72C6EFA4B396728Data[0].cDoctypeId = cDoctypeId;
    objectHeader0950DF758066453DA72C6EFA4B396728Data[0].calculatePromotions = calculatePromotions;
    objectHeader0950DF758066453DA72C6EFA4B396728Data[0].cProjectId = cProjectId;
    objectHeader0950DF758066453DA72C6EFA4B396728Data[0].cCostcenterId = cCostcenterId;
    objectHeader0950DF758066453DA72C6EFA4B396728Data[0].aAssetId = aAssetId;
    objectHeader0950DF758066453DA72C6EFA4B396728Data[0].cCampaignId = cCampaignId;
    objectHeader0950DF758066453DA72C6EFA4B396728Data[0].cCampaignIdr = "";
    objectHeader0950DF758066453DA72C6EFA4B396728Data[0].user1Id = user1Id;
    objectHeader0950DF758066453DA72C6EFA4B396728Data[0].user2Id = user2Id;
    objectHeader0950DF758066453DA72C6EFA4B396728Data[0].emAtecfeMenobserrorSri = emAtecfeMenobserrorSri;
    objectHeader0950DF758066453DA72C6EFA4B396728Data[0].emAtecfeCodigoAcc = emAtecfeCodigoAcc;
    objectHeader0950DF758066453DA72C6EFA4B396728Data[0].withholdingamount = withholdingamount;
    objectHeader0950DF758066453DA72C6EFA4B396728Data[0].cInvoiceId = cInvoiceId;
    objectHeader0950DF758066453DA72C6EFA4B396728Data[0].isdiscountprinted = isdiscountprinted;
    objectHeader0950DF758066453DA72C6EFA4B396728Data[0].isprinted = isprinted;
    objectHeader0950DF758066453DA72C6EFA4B396728Data[0].lastcalculatedondate = lastcalculatedondate;
    objectHeader0950DF758066453DA72C6EFA4B396728Data[0].isactive = isactive;
    objectHeader0950DF758066453DA72C6EFA4B396728Data[0].processing = processing;
    objectHeader0950DF758066453DA72C6EFA4B396728Data[0].isselfservice = isselfservice;
    objectHeader0950DF758066453DA72C6EFA4B396728Data[0].processed = processed;
    objectHeader0950DF758066453DA72C6EFA4B396728Data[0].istaxincluded = istaxincluded;
    objectHeader0950DF758066453DA72C6EFA4B396728Data[0].generateto = generateto;
    objectHeader0950DF758066453DA72C6EFA4B396728Data[0].cWithholdingId = cWithholdingId;
    objectHeader0950DF758066453DA72C6EFA4B396728Data[0].paymentrule = paymentrule;
    objectHeader0950DF758066453DA72C6EFA4B396728Data[0].adClientId = adClientId;
    objectHeader0950DF758066453DA72C6EFA4B396728Data[0].dateprinted = dateprinted;
    objectHeader0950DF758066453DA72C6EFA4B396728Data[0].issotrx = issotrx;
    objectHeader0950DF758066453DA72C6EFA4B396728Data[0].language = "";
    return objectHeader0950DF758066453DA72C6EFA4B396728Data;
  }

/**
Select for auxiliar field
 */
  public static String selectDef0445FEDA79A747C48A7B3EF852D176DC(ConnectionProvider connectionProvider, String AD_Org_ID, String AD_Role_ID, String AD_Client_ID)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT FIN_Payment_Priority_ID FROM FIN_Payment_Priority WHERE AD_ISORGINCLUDED( (CASE WHEN ?='0' THEN (SELECT ad_org_id FROM ad_org WHERE name = ( SELECT min(o.name) FROM ad_org o join ad_orgtype ot on (o.ad_orgtype_id=ot.ad_orgtype_id) join ad_role_orgaccess ra on (o.ad_org_id=ra.ad_org_id) join ad_role r on (ra.ad_role_id = r.ad_role_id) WHERE ot.istransactionsallowed = 'Y' and r.ad_role_id=?)) ELSE ? END), AD_Org_ID, ?) <> -1 AND Isdefault = 'Y' ORDER BY Priority ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, AD_Org_ID);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, AD_Role_ID);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, AD_Org_ID);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, AD_Client_ID);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "fin_payment_priority_id");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDef3489_0(ConnectionProvider connectionProvider, String CreatedByR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as CreatedBy FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, CreatedByR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "createdby");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDef3491_1(ConnectionProvider connectionProvider, String UpdatedByR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as UpdatedBy FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, UpdatedByR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "updatedby");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDef3499_2(ConnectionProvider connectionProvider, String C_BPartner_IDR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as C_BPartner_ID FROM C_BPartner left join (select C_BPartner_ID, Name from C_BPartner) table2 on (C_BPartner.C_BPartner_ID = table2.C_BPartner_ID) WHERE C_BPartner.isActive='Y' AND C_BPartner.C_BPartner_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, C_BPartner_IDR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "c_bpartner_id");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for action search
 */
  public static String selectActDefM_Locator_ID(ConnectionProvider connectionProvider, String M_Locator_ID)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT Value FROM M_Locator WHERE isActive='Y' AND M_Locator_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, M_Locator_ID);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "value");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for action search
 */
  public static String selectActDefC_Invoice_ID(ConnectionProvider connectionProvider, String C_Invoice_ID)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT DocumentNo FROM C_Invoice WHERE isActive='Y' AND C_Invoice_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, C_Invoice_ID);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "documentno");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public int update(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        UPDATE C_Invoice" +
      "        SET AD_Org_ID = (?) , C_DocTypeTarget_ID = (?) , EM_Co_Nro_Estab = (?) , EM_Co_Punto_Emision = (?) , FIN_Payment_Priority_ID = (?) , DocumentNo = (?) , EM_Co_Nro_Aut_Sri = (?) , EM_Co_Vencimiento_Aut_Sri = TO_DATE(?) , EM_Atecfe_Fecha_Sri = TO_TIMESTAMP(?, ?) , DateInvoiced = TO_DATE(?) , C_BPartner_ID = (?) , C_BPartner_Location_ID = (?) , Description = (?) , C_PaymentTerm_ID = (?) , FIN_Paymentmethod_ID = (?) , DocStatus = (?) , EM_Atecfe_Docstatus = (?) , EM_Atecfe_Totaldescuento = TO_NUMBER(?) , GrandTotal = TO_NUMBER(?) , TotalLines = TO_NUMBER(?) , EM_Atecfe_C_Invoice_ID = (?) , C_Currency_ID = (?) , Ispaid = (?) , AD_User_ID = (?) , Totalpaid = TO_NUMBER(?) , EM_APRM_Addpayment = (?) , Posted = (?) , EM_APRM_Processinvoice = (?) , EM_Atecfe_Docaction = (?) , DocAction = (?) , CreateFrom = (?) , EM_Atecfe_Imp_Xml = (?) , CopyFrom = (?) , EM_Atecfe_Imp_Pdf = (?) , OutstandingAmt = TO_NUMBER(?) , DueAmt = TO_NUMBER(?) , DaysTillDue = TO_NUMBER(?) , Percentageoverdue = TO_NUMBER(?) , AD_OrgTrx_ID = (?) , Finalsettlement = TO_DATE(?) , Daysoutstanding = TO_NUMBER(?) , M_PriceList_ID = (?) , SalesRep_ID = (?) , C_Order_ID = (?) , POReference = (?) , DateAcct = TO_DATE(?) , UpdatePaymentMonitor = (?) , Taxdate = TO_DATE(?) , C_Activity_ID = (?) , C_Charge_ID = (?) , ChargeAmt = TO_NUMBER(?) , DateOrdered = TO_DATE(?) , C_DocType_ID = (?) , Calculate_Promotions = (?) , C_Project_ID = (?) , C_Costcenter_ID = (?) , A_Asset_ID = (?) , C_Campaign_ID = (?) , User1_ID = (?) , User2_ID = (?) , Em_Atecfe_Menobserror_Sri = (?) , EM_Atecfe_Codigo_Acc = (?) , C_Invoice_ID = (?) , IsDiscountPrinted = (?) , LastCalculatedOnDate = TO_DATE(?) , IsSelfService = (?) , IsTaxIncluded = (?) , PaymentRule = (?) , C_Withholding_ID = (?) , AD_Client_ID = (?) , DatePrinted = TO_DATE(?) , IsSOTrx = (?) , GenerateTo = (?) , IsPrinted = (?) , IsActive = (?) , Processed = (?) , Processing = (?) , Withholdingamount = TO_NUMBER(?) , updated = now(), updatedby = ? " +
      "        WHERE C_Invoice.C_Invoice_ID = ? " +
      "        AND C_Invoice.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND C_Invoice.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cDoctypetargetId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emCoNroEstab);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emCoPuntoEmision);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, finPaymentPriorityId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, documentno);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emCoNroAutSri);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emCoVencimientoAutSri);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAtecfeFechaSri);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateinvoiced);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cBpartnerId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cBpartnerLocationId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, description);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cPaymenttermId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, finPaymentmethodId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, docstatus);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAtecfeDocstatus);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAtecfeTotaldescuento);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, grandtotal);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, totallines);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAtecfeCInvoiceId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cCurrencyId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ispaid);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adUserId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, totalpaid);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAprmAddpayment);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, posted);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAprmProcessinvoice);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAtecfeDocaction);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, docaction);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, createfrom);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAtecfeImpXml);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, copyfrom);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAtecfeImpPdf);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, outstandingamt);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dueamt);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, daystilldue);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, percentageoverdue);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgtrxId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, finalsettlement);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, daysoutstanding);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mPricelistId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, salesrepId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cOrderId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, poreference);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateacct);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatepaymentmonitor);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, taxdate);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cActivityId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cChargeId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, chargeamt);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateordered);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cDoctypeId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, calculatePromotions);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cProjectId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cCostcenterId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, aAssetId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cCampaignId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, user1Id);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, user2Id);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAtecfeMenobserrorSri);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAtecfeCodigoAcc);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cInvoiceId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isdiscountprinted);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, lastcalculatedondate);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isselfservice);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, istaxincluded);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paymentrule);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cWithholdingId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateprinted);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, issotrx);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, generateto);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isprinted);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isactive);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, processed);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, processing);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, withholdingamount);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cInvoiceId);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public int insert(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        INSERT INTO C_Invoice " +
      "        (AD_Org_ID, C_DocTypeTarget_ID, EM_Co_Nro_Estab, EM_Co_Punto_Emision, FIN_Payment_Priority_ID, DocumentNo, EM_Co_Nro_Aut_Sri, EM_Co_Vencimiento_Aut_Sri, EM_Atecfe_Fecha_Sri, DateInvoiced, C_BPartner_ID, C_BPartner_Location_ID, Description, C_PaymentTerm_ID, FIN_Paymentmethod_ID, DocStatus, EM_Atecfe_Docstatus, EM_Atecfe_Totaldescuento, GrandTotal, EM_Atecfe_C_Invoice_ID, TotalLines, C_Currency_ID, Ispaid, AD_User_ID, Totalpaid, EM_APRM_Addpayment, Posted, EM_APRM_Processinvoice, EM_Atecfe_Docaction, DocAction, CreateFrom, CopyFrom, EM_Atecfe_Imp_Xml, EM_Atecfe_Imp_Pdf, OutstandingAmt, DueAmt, DaysTillDue, Percentageoverdue, AD_OrgTrx_ID, Finalsettlement, Daysoutstanding, M_PriceList_ID, SalesRep_ID, C_Order_ID, POReference, DateAcct, UpdatePaymentMonitor, Taxdate, C_Activity_ID, C_Charge_ID, ChargeAmt, DateOrdered, C_DocType_ID, Calculate_Promotions, C_Project_ID, C_Costcenter_ID, A_Asset_ID, C_Campaign_ID, User1_ID, User2_ID, Em_Atecfe_Menobserror_Sri, EM_Atecfe_Codigo_Acc, Withholdingamount, C_Invoice_ID, IsDiscountPrinted, IsPrinted, LastCalculatedOnDate, IsActive, Processing, IsSelfService, Processed, IsTaxIncluded, GenerateTo, C_Withholding_ID, PaymentRule, AD_Client_ID, DatePrinted, IsSOTrx, created, createdby, updated, updatedBy)" +
      "        VALUES ((?), (?), (?), (?), (?), (?), (?), TO_DATE(?), TO_TIMESTAMP(?, ?), TO_DATE(?), (?), (?), (?), (?), (?), (?), (?), TO_NUMBER(?), TO_NUMBER(?), (?), TO_NUMBER(?), (?), (?), (?), TO_NUMBER(?), (?), (?), (?), (?), (?), (?), (?), (?), (?), TO_NUMBER(?), TO_NUMBER(?), TO_NUMBER(?), TO_NUMBER(?), (?), TO_DATE(?), TO_NUMBER(?), (?), (?), (?), (?), TO_DATE(?), (?), TO_DATE(?), (?), (?), TO_NUMBER(?), TO_DATE(?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), TO_NUMBER(?), (?), (?), (?), TO_DATE(?), (?), (?), (?), (?), (?), (?), (?), (?), (?), TO_DATE(?), (?), now(), ?, now(), ?)";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cDoctypetargetId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emCoNroEstab);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emCoPuntoEmision);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, finPaymentPriorityId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, documentno);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emCoNroAutSri);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emCoVencimientoAutSri);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAtecfeFechaSri);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateinvoiced);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cBpartnerId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cBpartnerLocationId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, description);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cPaymenttermId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, finPaymentmethodId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, docstatus);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAtecfeDocstatus);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAtecfeTotaldescuento);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, grandtotal);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAtecfeCInvoiceId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, totallines);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cCurrencyId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ispaid);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adUserId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, totalpaid);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAprmAddpayment);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, posted);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAprmProcessinvoice);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAtecfeDocaction);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, docaction);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, createfrom);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, copyfrom);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAtecfeImpXml);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAtecfeImpPdf);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, outstandingamt);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dueamt);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, daystilldue);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, percentageoverdue);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgtrxId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, finalsettlement);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, daysoutstanding);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mPricelistId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, salesrepId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cOrderId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, poreference);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateacct);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatepaymentmonitor);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, taxdate);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cActivityId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cChargeId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, chargeamt);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateordered);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cDoctypeId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, calculatePromotions);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cProjectId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cCostcenterId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, aAssetId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cCampaignId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, user1Id);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, user2Id);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAtecfeMenobserrorSri);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAtecfeCodigoAcc);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, withholdingamount);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cInvoiceId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isdiscountprinted);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isprinted);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, lastcalculatedondate);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isactive);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, processing);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isselfservice);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, processed);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, istaxincluded);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, generateto);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cWithholdingId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paymentrule);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateprinted);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, issotrx);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, createdby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public static int delete(ConnectionProvider connectionProvider, String param1, String adUserClient, String adOrgClient)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        DELETE FROM C_Invoice" +
      "        WHERE C_Invoice.C_Invoice_ID = ? " +
      "        AND C_Invoice.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND C_Invoice.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, param1);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

/**
Select for relation
 */
  public static String selectOrg(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT AD_ORG_ID" +
      "          FROM C_Invoice" +
      "         WHERE C_Invoice.C_Invoice_ID = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "ad_org_id");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public static String getCurrentDBTimestamp(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp" +
      "          FROM C_Invoice" +
      "         WHERE C_Invoice.C_Invoice_ID = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "updated_time_stamp");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }
}
