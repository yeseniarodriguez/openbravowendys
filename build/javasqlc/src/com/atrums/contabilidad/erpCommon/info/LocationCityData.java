//Sqlc generated V1.O00-1
package com.atrums.contabilidad.erpCommon.info;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import java.util.*;

class LocationCityData implements FieldProvider {
static Logger log4j = Logger.getLogger(LocationCityData.class);
  private String InitRecordNumber="0";
  public String id;
  public String name;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("id"))
      return id;
    else if (fieldName.equalsIgnoreCase("name"))
      return name;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

  public static LocationCityData[] select(ConnectionProvider connectionProvider, String cRegionId)    throws ServletException {
    return select(connectionProvider, cRegionId, 0, 0);
  }

  public static LocationCityData[] select(ConnectionProvider connectionProvider, String cRegionId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT c_city_id as ID,name  FROM C_City      " +
      "        WHERE C_City.C_Region_ID=?";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cRegionId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        LocationCityData objectLocationCityData = new LocationCityData();
        objectLocationCityData.id = UtilSql.getValue(result, "id");
        objectLocationCityData.name = UtilSql.getValue(result, "name");
        objectLocationCityData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectLocationCityData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    LocationCityData objectLocationCityData[] = new LocationCityData[vector.size()];
    vector.copyInto(objectLocationCityData);
    return(objectLocationCityData);
  }
}
