//Sqlc generated V1.O00-1
package com.atrums.contabilidad.ad_callouts;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import java.util.*;

class COValorRetencionLineaCompraData implements FieldProvider {
static Logger log4j = Logger.getLogger(COValorRetencionLineaCompraData.class);
  private String InitRecordNumber="0";
  public String porcentaje;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("porcentaje"))
      return porcentaje;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

  public static COValorRetencionLineaCompraData[] select(ConnectionProvider connectionProvider, String coBpRetencionCompraId)    throws ServletException {
    return select(connectionProvider, coBpRetencionCompraId, 0, 0);
  }

  public static COValorRetencionLineaCompraData[] select(ConnectionProvider connectionProvider, String coBpRetencionCompraId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT r.porcentaje " +
      "        FROM co_bp_retencion_compra b,co_tipo_retencion r " +
      "        WHERE b.co_tipo_retencion_id=r.co_tipo_retencion_id " +
      "        AND b.co_bp_retencion_compra_id=?";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, coBpRetencionCompraId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        COValorRetencionLineaCompraData objectCOValorRetencionLineaCompraData = new COValorRetencionLineaCompraData();
        objectCOValorRetencionLineaCompraData.porcentaje = UtilSql.getValue(result, "porcentaje");
        objectCOValorRetencionLineaCompraData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectCOValorRetencionLineaCompraData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    COValorRetencionLineaCompraData objectCOValorRetencionLineaCompraData[] = new COValorRetencionLineaCompraData[vector.size()];
    vector.copyInto(objectCOValorRetencionLineaCompraData);
    return(objectCOValorRetencionLineaCompraData);
  }
}
