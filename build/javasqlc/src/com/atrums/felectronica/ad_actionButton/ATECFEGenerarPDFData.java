//Sqlc generated V1.O00-1
package com.atrums.felectronica.ad_actionButton;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import java.util.*;

class ATECFEGenerarPDFData implements FieldProvider {
static Logger log4j = Logger.getLogger(ATECFEGenerarPDFData.class);
  private String InitRecordNumber="0";
  public String dato1;
  public String dato2;
  public String dato3;
  public String dato4;
  public String dato5;
  public String dato6;
  public String dato7;
  public String dato8;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("dato1"))
      return dato1;
    else if (fieldName.equalsIgnoreCase("dato2"))
      return dato2;
    else if (fieldName.equalsIgnoreCase("dato3"))
      return dato3;
    else if (fieldName.equalsIgnoreCase("dato4"))
      return dato4;
    else if (fieldName.equalsIgnoreCase("dato5"))
      return dato5;
    else if (fieldName.equalsIgnoreCase("dato6"))
      return dato6;
    else if (fieldName.equalsIgnoreCase("dato7"))
      return dato7;
    else if (fieldName.equalsIgnoreCase("dato8"))
      return dato8;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

  public static ATECFEGenerarPDFData[] methodSeleccionardummy(ConnectionProvider connectionProvider, String cInvoiceId)    throws ServletException {
    return methodSeleccionardummy(connectionProvider, cInvoiceId, 0, 0);
  }

  public static ATECFEGenerarPDFData[] methodSeleccionardummy(ConnectionProvider connectionProvider, String cInvoiceId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "	  select d.dummy as dato1, " +
      "			 d.dummy as dato2, " +
      "			 d.dummy as dato3, " +
      "			 d.dummy as dato4," +
      "			 d.dummy as dato5," +
      "			 d.dummy as dato6," +
      "			 d.dummy as dato7," +
      "			 d.dummy as dato8" +
      "	  from dual d " +
      "	  where d.dummy = ?";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cInvoiceId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        ATECFEGenerarPDFData objectATECFEGenerarPDFData = new ATECFEGenerarPDFData();
        objectATECFEGenerarPDFData.dato1 = UtilSql.getValue(result, "dato1");
        objectATECFEGenerarPDFData.dato2 = UtilSql.getValue(result, "dato2");
        objectATECFEGenerarPDFData.dato3 = UtilSql.getValue(result, "dato3");
        objectATECFEGenerarPDFData.dato4 = UtilSql.getValue(result, "dato4");
        objectATECFEGenerarPDFData.dato5 = UtilSql.getValue(result, "dato5");
        objectATECFEGenerarPDFData.dato6 = UtilSql.getValue(result, "dato6");
        objectATECFEGenerarPDFData.dato7 = UtilSql.getValue(result, "dato7");
        objectATECFEGenerarPDFData.dato8 = UtilSql.getValue(result, "dato8");
        objectATECFEGenerarPDFData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectATECFEGenerarPDFData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    ATECFEGenerarPDFData objectATECFEGenerarPDFData[] = new ATECFEGenerarPDFData[vector.size()];
    vector.copyInto(objectATECFEGenerarPDFData);
    return(objectATECFEGenerarPDFData);
  }

  public static ATECFEGenerarPDFData[] methodSeleccionarInvo(ConnectionProvider connectionProvider, String cInvoiceId)    throws ServletException {
    return methodSeleccionarInvo(connectionProvider, cInvoiceId, 0, 0);
  }

  public static ATECFEGenerarPDFData[] methodSeleccionarInvo(ConnectionProvider connectionProvider, String cInvoiceId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "	  select encode(em_atecfe_documento_xml,'base64') as dato1, " +
      "			 em_atecfe_codigo_acc as dato2" +
      "	  from c_invoice " +
      "	  where c_invoice_id = ?";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cInvoiceId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        ATECFEGenerarPDFData objectATECFEGenerarPDFData = new ATECFEGenerarPDFData();
        objectATECFEGenerarPDFData.dato1 = UtilSql.getValue(result, "dato1");
        objectATECFEGenerarPDFData.dato2 = UtilSql.getValue(result, "dato2");
        objectATECFEGenerarPDFData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectATECFEGenerarPDFData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    ATECFEGenerarPDFData objectATECFEGenerarPDFData[] = new ATECFEGenerarPDFData[vector.size()];
    vector.copyInto(objectATECFEGenerarPDFData);
    return(objectATECFEGenerarPDFData);
  }

  public static ATECFEGenerarPDFData[] methodSeleccionarRete(ConnectionProvider connectionProvider, String cRetencionId)    throws ServletException {
    return methodSeleccionarRete(connectionProvider, cRetencionId, 0, 0);
  }

  public static ATECFEGenerarPDFData[] methodSeleccionarRete(ConnectionProvider connectionProvider, String cRetencionId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "	  select encode(em_atecfe_documento_xml,'base64') as dato1, " +
      "			 em_atecfe_codigo_acc as dato2" +
      "	  from co_retencion_compra" +
      "	  where co_retencion_compra_id = ?";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cRetencionId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        ATECFEGenerarPDFData objectATECFEGenerarPDFData = new ATECFEGenerarPDFData();
        objectATECFEGenerarPDFData.dato1 = UtilSql.getValue(result, "dato1");
        objectATECFEGenerarPDFData.dato2 = UtilSql.getValue(result, "dato2");
        objectATECFEGenerarPDFData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectATECFEGenerarPDFData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    ATECFEGenerarPDFData objectATECFEGenerarPDFData[] = new ATECFEGenerarPDFData[vector.size()];
    vector.copyInto(objectATECFEGenerarPDFData);
    return(objectATECFEGenerarPDFData);
  }

  public static ATECFEGenerarPDFData[] methodSeleccionarGuia(ConnectionProvider connectionProvider, String mInoutId)    throws ServletException {
    return methodSeleccionarGuia(connectionProvider, mInoutId, 0, 0);
  }

  public static ATECFEGenerarPDFData[] methodSeleccionarGuia(ConnectionProvider connectionProvider, String mInoutId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "	  select encode(em_atecfe_documento_xml,'base64') as dato1, " +
      "			 em_atecfe_codigo_acc as dato2" +
      "	  from m_inout" +
      "	  where m_inout_id = ?";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mInoutId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        ATECFEGenerarPDFData objectATECFEGenerarPDFData = new ATECFEGenerarPDFData();
        objectATECFEGenerarPDFData.dato1 = UtilSql.getValue(result, "dato1");
        objectATECFEGenerarPDFData.dato2 = UtilSql.getValue(result, "dato2");
        objectATECFEGenerarPDFData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectATECFEGenerarPDFData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    ATECFEGenerarPDFData objectATECFEGenerarPDFData[] = new ATECFEGenerarPDFData[vector.size()];
    vector.copyInto(objectATECFEGenerarPDFData);
    return(objectATECFEGenerarPDFData);
  }

  public static ATECFEGenerarPDFData[] methodSeleccionarDocumen(ConnectionProvider connectionProvider, String cInvoiceId)    throws ServletException {
    return methodSeleccionarDocumen(connectionProvider, cInvoiceId, 0, 0);
  }

  public static ATECFEGenerarPDFData[] methodSeleccionarDocumen(ConnectionProvider connectionProvider, String cInvoiceId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "	    select coalesce((CASE WHEN (upper(dt.name) like '%FACTURA%') THEN '01' WHEN (upper(dt.name) like '%NOTA%') THEN '04' WHEN (upper(dt.name) like '%RETENC%') THEN '07' END), '') as dato1, " +
      "			   i.em_atecfe_codigo_acc as dato2" +
      "		from c_invoice i" +
      "			join c_doctype dt on (i.c_doctypetarget_id = dt.c_doctype_id)" +
      "		where i.c_invoice_id = ?" +
      "		limit 1";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cInvoiceId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        ATECFEGenerarPDFData objectATECFEGenerarPDFData = new ATECFEGenerarPDFData();
        objectATECFEGenerarPDFData.dato1 = UtilSql.getValue(result, "dato1");
        objectATECFEGenerarPDFData.dato2 = UtilSql.getValue(result, "dato2");
        objectATECFEGenerarPDFData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectATECFEGenerarPDFData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    ATECFEGenerarPDFData objectATECFEGenerarPDFData[] = new ATECFEGenerarPDFData[vector.size()];
    vector.copyInto(objectATECFEGenerarPDFData);
    return(objectATECFEGenerarPDFData);
  }

  public static ATECFEGenerarPDFData[] methodSeleccionarDocumenRete(ConnectionProvider connectionProvider, String cRetencionId)    throws ServletException {
    return methodSeleccionarDocumenRete(connectionProvider, cRetencionId, 0, 0);
  }

  public static ATECFEGenerarPDFData[] methodSeleccionarDocumenRete(ConnectionProvider connectionProvider, String cRetencionId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "	    select em_atecfe_codigo_acc as dato1 " +
      "	    from co_retencion_compra" +
      "	    where co_retencion_compra_id = ?";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cRetencionId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        ATECFEGenerarPDFData objectATECFEGenerarPDFData = new ATECFEGenerarPDFData();
        objectATECFEGenerarPDFData.dato1 = UtilSql.getValue(result, "dato1");
        objectATECFEGenerarPDFData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectATECFEGenerarPDFData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    ATECFEGenerarPDFData objectATECFEGenerarPDFData[] = new ATECFEGenerarPDFData[vector.size()];
    vector.copyInto(objectATECFEGenerarPDFData);
    return(objectATECFEGenerarPDFData);
  }

  public static ATECFEGenerarPDFData[] methodSeleccionarDocumenGuia(ConnectionProvider connectionProvider, String mInoutId)    throws ServletException {
    return methodSeleccionarDocumenGuia(connectionProvider, mInoutId, 0, 0);
  }

  public static ATECFEGenerarPDFData[] methodSeleccionarDocumenGuia(ConnectionProvider connectionProvider, String mInoutId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "	    select em_atecfe_codigo_acc as dato1 " +
      "	    from m_inout" +
      "	    where m_inout_id = ?";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mInoutId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        ATECFEGenerarPDFData objectATECFEGenerarPDFData = new ATECFEGenerarPDFData();
        objectATECFEGenerarPDFData.dato1 = UtilSql.getValue(result, "dato1");
        objectATECFEGenerarPDFData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectATECFEGenerarPDFData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    ATECFEGenerarPDFData objectATECFEGenerarPDFData[] = new ATECFEGenerarPDFData[vector.size()];
    vector.copyInto(objectATECFEGenerarPDFData);
    return(objectATECFEGenerarPDFData);
  }
}
