package com.atrums.felectronica.ad_actionButton;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.jfree.util.Log;
import org.openbravo.base.secureApp.HttpSecureAppServlet;
import org.openbravo.base.secureApp.VariablesSecureApp;

import com.atrums.felectronica.process.ATECFE_Funciones_Aux;

public class ATECFE_ImprimirPDF extends HttpSecureAppServlet {

  private static final long serialVersionUID = 1L;

  ATECFE_Funciones_Aux opeaux = new ATECFE_Funciones_Aux();

  public void init(ServletConfig config) {
    super.init(config);
    boolHist = false;
  }

  public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException,
      ServletException {
    VariablesSecureApp vars = new VariablesSecureApp(request);
    String strInvoice = vars.getStringParameter("inpcInvoiceId");
    String strRetencion = vars.getStringParameter("inpcoRetencionCompraId");
    String strGuia = vars.getStringParameter("inpmInoutId");

    if (strGuia != null) {
      Log.info("guia: " + strGuia);
    }

    if (!strInvoice.equals("") && strInvoice != null && !strInvoice.equals("null")) {
      imprPDF(response, strInvoice);
    } else if (!strRetencion.equals("") && strRetencion != null && !strRetencion.equals("null")) {
      imprPDFRet(response, strRetencion);
    } else if (!strGuia.equals("") && strGuia != null && !strGuia.equals("null")) {
      imprPDFGuia(response, strGuia);
    } else {
      response.setContentType("text/html; charset=UTF-8");
      PrintWriter out = response.getWriter();
      out.println("");
      out.close();
    }
  }

  protected void imprPDF(HttpServletResponse response, String strInvoice) throws IOException,
      ServletException {

    ATECFEGenerarPDFData[] atfData = ATECFEGenerarPDFData
        .methodSeleccionarDocumen(this, strInvoice);

    File flPdf = null;

    if (atfData.length > 0) {
      if (atfData[0].dato1.equals("1") || atfData[0].dato1.equals("01")) {
        flPdf = opeaux.generarPDF(this,
            "@basedesign@/com/atrums/felectronica/erpReport/Rpt_Factura.jrxml", "Factura",
            strInvoice);
      } else if (atfData[0].dato1.equals("4") || atfData[0].dato1.equals("04")) {
        flPdf = opeaux.generarPDF(this,
            "@basedesign@/com/atrums/felectronica/erpReport/Rpt_NotaCredito.jrxml", "Nota_Credito",
            strInvoice);
      } else if (atfData[0].dato1.equals("5") || atfData[0].dato1.equals("05")) {
        flPdf = opeaux.generarPDF(this,
            "@basedesign@/com/atrums/felectronica/erpReport/Rpt_NotaDebito.jrxml", "Nota_Debito",
            strInvoice);
      }
    }

    if (flPdf != null) {
      FileInputStream insStream = new FileInputStream(flPdf);

      response.setContentType("application/pdf");
      response.setHeader("Content-Disposition", "attachment; filename=Documento_"
          + atfData[0].dato2 + ".pdf");
      IOUtils.copy(insStream, response.getOutputStream());
      flPdf.exists();
      insStream.close();
      response.flushBuffer();
    }
  }

  protected void imprPDFRet(HttpServletResponse response, String strRetencion) throws IOException,
      ServletException {

    File flPdf = null;

    flPdf = opeaux.generarPDF(this,
        "@basedesign@/com/atrums/felectronica/erpReport/Rpt_Retenciones.jrxml", "Retencion",
        strRetencion);

    if (flPdf != null) {
      FileInputStream insStream = new FileInputStream(flPdf);

      ATECFEGenerarPDFData[] atfData = ATECFEGenerarPDFData.methodSeleccionarDocumenRete(this,
          strRetencion);
      response.setContentType("application/pdf");
      response.setHeader("Content-Disposition", "attachment; filename=Documento_"
          + atfData[0].dato1 + ".pdf");
      IOUtils.copy(insStream, response.getOutputStream());
      flPdf.exists();
      insStream.close();
      response.flushBuffer();
    }
  }

  protected void imprPDFGuia(HttpServletResponse response, String strGuia) throws IOException,
      ServletException {

    File flPdf = null;

    Log.info("Imprimiendo PDF Guia");
    flPdf = opeaux.generarPDF(this,
        "@basedesign@/com/atrums/felectronica/erpReport/Rpt_Guias.jrxml", "Guia", strGuia);

    if (flPdf != null) {
      FileInputStream insStream = new FileInputStream(flPdf);

      ATECFEGenerarPDFData[] atfData = ATECFEGenerarPDFData.methodSeleccionarDocumenGuia(this,
          strGuia);
      response.setContentType("application/pdf");
      response.setHeader("Content-Disposition", "attachment; filename=Documento_"
          + atfData[0].dato1 + ".pdf");
      IOUtils.copy(insStream, response.getOutputStream());
      flPdf.exists();
      insStream.close();
      response.flushBuffer();
    }
  }
}
