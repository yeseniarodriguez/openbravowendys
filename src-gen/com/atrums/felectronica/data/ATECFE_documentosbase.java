/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2008-2014 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 ************************************************************************
*/
package com.atrums.felectronica.data;

import java.util.Date;

import org.openbravo.base.structure.ActiveEnabled;
import org.openbravo.base.structure.BaseOBObject;
import org.openbravo.base.structure.ClientEnabled;
import org.openbravo.base.structure.OrganizationEnabled;
import org.openbravo.base.structure.Traceable;
import org.openbravo.model.ad.access.User;
import org.openbravo.model.ad.system.Client;
import org.openbravo.model.common.enterprise.Organization;
/**
 * Entity class for entity ATECFE_documentosbase (stored in table ATECFE_documentosbase).
 *
 * NOTE: This class should not be instantiated directly. To instantiate this
 * class the {@link org.openbravo.base.provider.OBProvider} should be used.
 */
public class ATECFE_documentosbase extends BaseOBObject implements Traceable, ClientEnabled, OrganizationEnabled, ActiveEnabled {
    private static final long serialVersionUID = 1L;
    public static final String TABLE_NAME = "ATECFE_documentosbase";
    public static final String ENTITY_NAME = "ATECFE_documentosbase";
    public static final String PROPERTY_ID = "id";
    public static final String PROPERTY_CLIENT = "client";
    public static final String PROPERTY_ORGANIZATION = "organization";
    public static final String PROPERTY_ACTIVE = "active";
    public static final String PROPERTY_CREATIONDATE = "creationDate";
    public static final String PROPERTY_CREATEDBY = "createdBy";
    public static final String PROPERTY_UPDATED = "updated";
    public static final String PROPERTY_UPDATEDBY = "updatedBy";
    public static final String PROPERTY_NRODOCUMENTO = "nrodocumento";
    public static final String PROPERTY_NROESTABLECIMIENTO = "nroestablecimiento";
    public static final String PROPERTY_NROEMISION = "nroemision";
    public static final String PROPERTY_TIPODOCUMENTO = "tipodocumento";
    public static final String PROPERTY_FECHADOCUMENTO = "fechadocumento";
    public static final String PROPERTY_FECHA = "fecha";
    public static final String PROPERTY_CLAVEACCESO = "claveacceso";
    public static final String PROPERTY_NROAUTORIZACION = "nroautorizacion";
    public static final String PROPERTY_FECHAAUTORIZACION = "fechaautorizacion";
    public static final String PROPERTY_USUARIO = "usuario";
    public static final String PROPERTY_CLAVE = "clave";
    public static final String PROPERTY_ESTADO = "estado";
    public static final String PROPERTY_MENSAJEERROR = "mensajeerror";

    public ATECFE_documentosbase() {
        setDefaultValue(PROPERTY_ACTIVE, true);
        setDefaultValue(PROPERTY_TIPODOCUMENTO, "FC");
        setDefaultValue(PROPERTY_ESTADO, "N");
    }

    @Override
    public String getEntityName() {
        return ENTITY_NAME;
    }

    public String getId() {
        return (String) get(PROPERTY_ID);
    }

    public void setId(String id) {
        set(PROPERTY_ID, id);
    }

    public Client getClient() {
        return (Client) get(PROPERTY_CLIENT);
    }

    public void setClient(Client client) {
        set(PROPERTY_CLIENT, client);
    }

    public Organization getOrganization() {
        return (Organization) get(PROPERTY_ORGANIZATION);
    }

    public void setOrganization(Organization organization) {
        set(PROPERTY_ORGANIZATION, organization);
    }

    public Boolean isActive() {
        return (Boolean) get(PROPERTY_ACTIVE);
    }

    public void setActive(Boolean active) {
        set(PROPERTY_ACTIVE, active);
    }

    public Date getCreationDate() {
        return (Date) get(PROPERTY_CREATIONDATE);
    }

    public void setCreationDate(Date creationDate) {
        set(PROPERTY_CREATIONDATE, creationDate);
    }

    public User getCreatedBy() {
        return (User) get(PROPERTY_CREATEDBY);
    }

    public void setCreatedBy(User createdBy) {
        set(PROPERTY_CREATEDBY, createdBy);
    }

    public Date getUpdated() {
        return (Date) get(PROPERTY_UPDATED);
    }

    public void setUpdated(Date updated) {
        set(PROPERTY_UPDATED, updated);
    }

    public User getUpdatedBy() {
        return (User) get(PROPERTY_UPDATEDBY);
    }

    public void setUpdatedBy(User updatedBy) {
        set(PROPERTY_UPDATEDBY, updatedBy);
    }

    public String getNrodocumento() {
        return (String) get(PROPERTY_NRODOCUMENTO);
    }

    public void setNrodocumento(String nrodocumento) {
        set(PROPERTY_NRODOCUMENTO, nrodocumento);
    }

    public String getNroestablecimiento() {
        return (String) get(PROPERTY_NROESTABLECIMIENTO);
    }

    public void setNroestablecimiento(String nroestablecimiento) {
        set(PROPERTY_NROESTABLECIMIENTO, nroestablecimiento);
    }

    public String getNroemision() {
        return (String) get(PROPERTY_NROEMISION);
    }

    public void setNroemision(String nroemision) {
        set(PROPERTY_NROEMISION, nroemision);
    }

    public String getTipodocumento() {
        return (String) get(PROPERTY_TIPODOCUMENTO);
    }

    public void setTipodocumento(String tipodocumento) {
        set(PROPERTY_TIPODOCUMENTO, tipodocumento);
    }

    public String getFechadocumento() {
        return (String) get(PROPERTY_FECHADOCUMENTO);
    }

    public void setFechadocumento(String fechadocumento) {
        set(PROPERTY_FECHADOCUMENTO, fechadocumento);
    }

    public Date getFecha() {
        return (Date) get(PROPERTY_FECHA);
    }

    public void setFecha(Date fecha) {
        set(PROPERTY_FECHA, fecha);
    }

    public String getClaveacceso() {
        return (String) get(PROPERTY_CLAVEACCESO);
    }

    public void setClaveacceso(String claveacceso) {
        set(PROPERTY_CLAVEACCESO, claveacceso);
    }

    public String getNroautorizacion() {
        return (String) get(PROPERTY_NROAUTORIZACION);
    }

    public void setNroautorizacion(String nroautorizacion) {
        set(PROPERTY_NROAUTORIZACION, nroautorizacion);
    }

    public String getFechaautorizacion() {
        return (String) get(PROPERTY_FECHAAUTORIZACION);
    }

    public void setFechaautorizacion(String fechaautorizacion) {
        set(PROPERTY_FECHAAUTORIZACION, fechaautorizacion);
    }

    public String getUsuario() {
        return (String) get(PROPERTY_USUARIO);
    }

    public void setUsuario(String usuario) {
        set(PROPERTY_USUARIO, usuario);
    }

    public String getClave() {
        return (String) get(PROPERTY_CLAVE);
    }

    public void setClave(String clave) {
        set(PROPERTY_CLAVE, clave);
    }

    public String getEstado() {
        return (String) get(PROPERTY_ESTADO);
    }

    public void setEstado(String estado) {
        set(PROPERTY_ESTADO, estado);
    }

    public String getMensajeerror() {
        return (String) get(PROPERTY_MENSAJEERROR);
    }

    public void setMensajeerror(String mensajeerror) {
        set(PROPERTY_MENSAJEERROR, mensajeerror);
    }

}
